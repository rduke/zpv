#include "stdafx.h"

#define TERRAIN_SIZE		33
#define TERRAIN_NB_VERTS	TERRAIN_SIZE*TERRAIN_SIZE
#define TERRAIN_NB_FACES	(TERRAIN_SIZE-1)*(TERRAIN_SIZE-1)*2
#define TERRAIN_OFFSET		0.0f
#define TERRAIN_WIDTH		20.0f
#define TERRAIN_CHAOS		70.0f //150.0f



namespace zpv{

	extern NxUserContactReport * carContactReport;


	PhysXManager::PhysXManager()
	{
		if ( !initSDK() )
			throw std::exception("nx sdk failed");
	}

	bool PhysXManager::initSDK()
	{
		materialDefault = 0;
		// Initialize PhysicsSDK
		NxPhysicsSDKDesc desc;
		NxSDKCreateError errorCode = NXCE_NO_ERROR;
		m_SDK = NxCreatePhysicsSDK(
				NX_PHYSICS_SDK_VERSION
			,	NULL
			,	&m_ErrorStream
			,	desc
			,	&errorCode
		);

		if( m_SDK == NULL ) 
		{
			printf("\nSDK create error (%d - %s).\nUnable to initialize the PhysX SDK, exiting the sample.\n\n", errorCode, getNxSDKCreateError(errorCode));
			return false;
		}

		if ( !CookingManager::getSingletonRef().initCooking( NULL, &m_ErrorStream ) )
		{
			printf("\nError: Unable to initialize the cooking library, exiting the sample.\n\n");
			return false;
		}

		m_SDK->setParameter(NX_SKIN_WIDTH, 0.025f);
		//enable visualisation
		m_SDK->setParameter(NX_VISUALIZATION_SCALE, 1.0f);

		m_SDK->setParameter(NX_VISUALIZE_BODY_AXES, 1.0f);
		m_SDK->setParameter(NX_VISUALIZE_COLLISION_SHAPES, 1.0f);
		m_SDK->setParameter(NX_VISUALIZE_COLLISION_AXES, 1.0f);
		m_SDK->setParameter(NX_VISUALIZE_CONTACT_POINT, 1.0f);
		m_SDK->setParameter(NX_VISUALIZE_CONTACT_NORMAL, 1.0f);

		m_SDK->setParameter(NX_VISUALIZE_JOINT_LOCAL_AXES, 1.0f);
		m_SDK->setParameter(NX_VISUALIZE_JOINT_WORLD_AXES, 1.0f);
		m_SDK->setParameter(NX_VISUALIZE_JOINT_LIMITS, 1.0f);

		// Don't slow down jointed objects
		m_SDK->setParameter(NX_ADAPTIVE_FORCE, 0.0f);
		
		//create some materials -- note that we reuse the same NxMaterial every time,
		//as it gets copied to the SDK with every setMaterial() call, not just referenced.
		
		// Create a scene
		NxSceneDesc sceneDesc;
		sceneDesc.gravity				= NxVec3(0.0f, -10.0f, 0.0f);
		sceneDesc.userContactReport		= carContactReport;
		m_Scene = m_SDK->createScene(sceneDesc);
		if(m_Scene == NULL) 
		{
			printf("\nError: Unable to create a PhysX scene, exiting the sample.\n\n");
			return false;
		}
		
		//default material
		NxMaterial * defaultMaterial = m_Scene->getMaterialFromIndex(0); 
		defaultMaterial->setRestitution(0.0f);
		defaultMaterial->setStaticFriction(0.8f);
		defaultMaterial->setDynamicFriction(0.8f);

		//create ground plane
		NxActorDesc actorDesc;
		//embed some simple shapes in terrain to make sure we can drive on them:
		NxBoxShapeDesc boxDesc;
		boxDesc.dimensions.set(10,1,5);
		boxDesc.localPose.t.set(30,0,30);
		boxDesc.localPose.M.fromQuat(NxQuat(-30, NxVec3(0,0,1)));
		actorDesc.shapes.pushBack(&boxDesc);
		m_Scene->createActor(actorDesc);

		actorDesc.setToDefault();
		NxSphereShapeDesc sphereShape;
		sphereShape.radius = 20;
		sphereShape.localPose.t.set(140, -18, 0);
		actorDesc.shapes.pushBack(&sphereShape);
		m_Scene->createActor(actorDesc);

		actorDesc.setToDefault();
		NxCapsuleShapeDesc capsuleShape;
		capsuleShape.radius = 10.0f;
		capsuleShape.height = 10.0f;
		capsuleShape.localPose.t.set(100, -8, 0);
		capsuleShape.localPose.M.setColumn(1, NxVec3(0,0,1));
		capsuleShape.localPose.M.setColumn(2, NxVec3(0,-1,0));
		actorDesc.shapes.pushBack(&capsuleShape);

		m_Scene->createActor(actorDesc);

		// Turn on all contact notifications:
		m_Scene->setActorGroupPairFlags(0, 0, NX_NOTIFY_ON_TOUCH);

		glColor4f(1.0f,1.0f,1.0f,1.0f);

		return true;
	}

	NxScene* PhysXManager::getDefaultScene()
	{
		return m_Scene;
	}

	NxPhysicsSDK* PhysXManager::getSDK()
	{
		return m_SDK;
	}

	void PhysXManager::releaseDefaultScene()
	{
		if( m_Scene )
			m_SDK->releaseScene( *m_Scene );
	}

	void PhysXManager::releaseSDK()
	{
		releaseDefaultScene();
		m_SDK->release();
	}

} // namespace zpv

