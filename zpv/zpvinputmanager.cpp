#include "stdafx.h"

namespace zpv
{

	void InputManager::appKey(unsigned char key, bool down)
	{
		if (!down)
			return;
		bool alt = (glutGetModifiers() & GLUT_ACTIVE_ALT) > 0;

		switch(key)
		{
			case 27:	exit(0); break;
			case 'p':	gPause = !gPause; break;
			case 'f':	if (VehicleManager::getActiveVehicle()) VehicleManager::getActiveVehicle()->standUp(); 
						break;
			case 'v':	gDebugVisualization = !gDebugVisualization; break;

			case '+':	if (VehicleManager::getActiveVehicle()) VehicleManager::getActiveVehicle()->gearUp(); 
						break;
			case '-':	if (VehicleManager::getActiveVehicle()) VehicleManager::getActiveVehicle()->gearDown(); 
						break;
			case 'e':
			{
				VehicleManager::selectNext();
			}
			break;
			case 'r':
			{
				NxVec3 t;
				NxVec3 vel;
				getCurrentPosAndDirection( t, vel );
				
				vel.normalize();
				vel*=30.0f;
				createCube( t, &vel );
			}
			break;
			case 'c':
			{
				if ( VehicleManager::getActiveVehicleNumber() < 0 )
				{
					VehicleManager::setActiveVehicle( gLastVehicleNumber );
				}
				else
				{
					gLastVehicleNumber = NxMath::max( 0, VehicleManager::getActiveVehicleNumber() );
					VehicleManager::setActiveVehicle( -1 );
				}
			}
			break;
		}
	}

	void InputManager::updateCamera( NxVec3& _cameraPos, NxVec3& _cameraDir )
	{
		CameraPos = _cameraPos;
		CameraDir = _cameraDir;
	}

	void InputManager::getCurrentPosAndDirection( NxVec3& pos, NxVec3& direction ) 
	{
		if( VehicleManager::getActiveVehicle() != NULL )
		{
			pos = CameraPos;
			direction = CameraDir;
		}
		else
		{
			pos = Eye;
			direction = Dir;
		}

	}

	void InputManager::callback_key( unsigned char c, int x, int y )
	{
		if( c >= 'A' && c <= 'Z' )
			c = c - 'A' + 'a';
		keyDown[c] = true;
		appKey(c, true);
	}

	void InputManager::callback_keyUp( unsigned char c, int x, int y )
	{
		if(c >= 'A' && c <= 'Z')
			c = c - 'A' + 'a';
		keyDown[c] = false;
		appKey(c,false);
	}

	void InputManager::createCube( const NxVec3& pos, const NxVec3* initial_velocity )
	{
		// Create body
		NxBodyDesc BodyDesc;
		if( initial_velocity )
			BodyDesc.linearVelocity = *initial_velocity;

		NxBoxShapeDesc boxDesc;
		NxReal size = 0.5f;
		boxDesc.dimensions		= NxVec3(size, size, size);
		BodyDesc.mass = 10.f;

		NxActorDesc ActorDesc;
		ActorDesc.shapes.pushBack(&boxDesc);
		ActorDesc.body			= &BodyDesc;
		ActorDesc.globalPose.t  = pos;
		PhysXManager::getSingletonRef().getDefaultScene()->createActor(ActorDesc);
	}

	void InputManager::simulate( float _timeStep )
	{
		NxScene* gScene = PhysXManager::getSingletonRef().getDefaultScene();
		if( gScene && !getPause() )
		{
			NxReal steering = 0;
			bool left = keyDown['a'] || keyDown[20];
			bool right = keyDown['d'] || keyDown[22];
			bool forward = keyDown['w'] || keyDown[21];
			bool backward = keyDown['s'] || keyDown[23];
			if( left && !right )
				steering = -1;
			else if( right && !left )
				steering = 1;
			NxReal acceleration = 0;
			if( forward && !backward )
				acceleration = 1;
			else if( backward && !forward )
				acceleration = -1;

			if( VehicleManager::getActiveVehicle() )
				VehicleManager::getActiveVehicle()->control( steering,
				                                             false,
															 acceleration,
															 false,
															 false );
			VehicleManager::updateAllVehicles( _timeStep );

			gScene->simulate( _timeStep );
			gScene->flushStream();
			gScene->fetchResults( NX_RIGID_BODY_FINISHED, true );
		}
	}

	void InputManager::cameraControls()
	{
		//camera controls:
		/*if ( keyDown['a'] || keyDown[20] )
			Eye -= N;
		if ( keyDown['d'] || keyDown[22] )
			Eye += N;*/
		if ( keyDown['w'] || keyDown[21] )
			Eye += Dir;
		if ( keyDown['s'] || keyDown[23] )
			Eye -= Dir;
	}

}