#include "stdafx.h"

namespace zpv
{

	std::set< AbstractVehicle* >	VehicleManager::_allVehicles;
	NxArray< AbstractVehicle*  >	VehicleManager::_allVehiclesSequential;
	std::set< AbstractVehicle* >	VehicleManager::_allChildVehicles;
	NxI32					VehicleManager::_activeVehicle = -1;
	AbstractVehicle*				VehicleManager::_activeVehicleP;

	void VehicleManager::AddVehicle(AbstractVehicle* v)
	{
		if (!isVehicle(v))
		{
			_allVehicles.insert(v);
			_allVehiclesSequential.pushBack(v);
		}
	}

	void VehicleManager::AddChildVehicle(AbstractVehicle* v)
	{
		if (!isVehicle(v))
		{
			_allChildVehicles.insert(v);
		}
	}

	void VehicleManager::updateAllVehicles(NxReal lastTimeStep)
	{
		std::set<AbstractVehicle*>::iterator it = _allVehicles.begin();
		for (;it != _allVehicles.end(); ++it)
		{
			(*it)->updateVehicle(lastTimeStep);
		}
		for (it = _allChildVehicles.begin(); it != _allChildVehicles.end(); ++it)
		{
			(*it)->updateVehicle(lastTimeStep);
		}
	}

	bool VehicleManager::isVehicle(AbstractVehicle* v)
	{
		std::set<AbstractVehicle*>::iterator it = _allVehicles.find(v);
		if (it != _allVehicles.end())
			return true;

		it = _allChildVehicles.find(v);
		return it != _allChildVehicles.end();
	}

	void VehicleManager::setActiveVehicleP()
	{
		if (_activeVehicle < 0)
		{
			_activeVehicleP = NULL;
			return;
		}
		_activeVehicleP = _allVehiclesSequential[_activeVehicle];
	}

	void VehicleManager::drawVehicles(bool debug)
	{
		for (size_t i = 0; i < _allVehiclesSequential.size(); i++)
		{
			_allVehiclesSequential[i]->draw(debug);
		}
		std::set<AbstractVehicle*>::iterator it = _allVehicles.begin();
		for (it = _allChildVehicles.begin(); it != _allChildVehicles.end(); ++it)
		{
			(*it)->draw(debug);
		}
	}

	void VehicleManager::selectNext()
	{
		_activeVehicle++;
		if (_activeVehicle >= (NxI32)_allVehicles.size())
			_activeVehicle = -1;
		setActiveVehicleP();
	}

	void VehicleManager::handlePair(NxContactPair& pair, int events)
	{
		if (isVehicle((AbstractVehicle*)pair.actors[0]->userData))
		{
			AbstractVehicle* v = (AbstractVehicle*)pair.actors[0]->userData;
			v->handleContactPair(pair, 0);
		}
		if (isVehicle((AbstractVehicle*)pair.actors[1]->userData))
		{
			AbstractVehicle* v = (AbstractVehicle*)pair.actors[1]->userData;
			v->handleContactPair(pair, 1);
		}
	}

}