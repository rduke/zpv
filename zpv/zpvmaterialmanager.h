#ifndef __ZPV_MATERIAL_MANAGER_H__
#define __ZPV_MATERIAL_MANAGER_H__

#include "ZpvMaterialManager.h"
#include <boost/unordered_map.hpp>

namespace zpv
{

	class MaterialManager
		: public Singleton< MaterialManager >
	{
	public:
		typedef boost::unordered_map< NxScene*, MaterialFactory* > MaterialFactoryMap;
		MaterialFactory* factoryForScene( NxScene* _scene = PhysXManager::getSingletonRef().getDefaultScene() );

	protected:
		friend class Singleton< MaterialManager >;
		~MaterialManager();

	private:
		MaterialFactoryMap m_Factories;
	};

}

#endif // __ZPV_MATERIAL_MANAGER_H__