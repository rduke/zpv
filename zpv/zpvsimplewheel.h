#ifndef __ZPV_SIMPLE_WHEEL_H__
#define __ZPV_SIMPLE_WHEEL_H__

#include "ZpvWheel.h"

namespace zpv
{

	#define GL_COLOR_TIRE 0.3f,0.3f,0.3f,1.0f
	#define GL_COLOR_TIRE_INNER 0.1f,0.1f,0.1f,1.0f

	class SimpleWheel
		: public Wheel
	{
		friend class Wheel;
	public:
		SimpleWheel( NxScene * _s );
		~SimpleWheel();

		virtual void tick( bool handbrake,
			               NxReal motorTorque,
					  	   NxReal brakeTorque,
						   NxReal dt );

		virtual NxActor* getTouchedActor() const
		{ 
			return contactInfo.otherActor;
		}

		virtual NxVec3			getWheelPos() const 
		{
			return wheelCapsule->getLocalPosition(); 
		}

		virtual void setAngle( NxReal angle );
		virtual void drawWheel( NxReal approx, bool debug = false ) const;

		virtual NxReal getRpm() const 
		{
			return NxMath::abs( _turnVelocity * 60.f ); 
		}

		virtual NxVec3 getGroundContactPos() const
		{
			return getWheelPos();
		}

		virtual float			getRadius() const 
		{
			return _radius; 
		}

		ContactInfo				contactInfo;
	private:

		void					getSteeringDirection( NxVec3& dir );
		void					updateContactPosition();
		void					updateAngularVelocity( NxReal lastTimeStepSize, bool handbrake );
		void					setWheelOrientation( const NxMat33& m )
		{
			wheelCapsule->setLocalOrientation( m );
			if( wheelConvex != NULL )
				wheelConvex->setLocalOrientation( m ); 
		}

		NxCapsuleShape*			wheelCapsule;
		NxConvexShape*			wheelConvex;
		NxScene *				scene;

		NxMaterial*				material;
		NxReal					_frictionToSide;
		NxReal					_frictionToFront;
		
		NxReal					_turnAngle;
		NxReal					_turnVelocity;
		NxReal					_radius;
		NxReal					_perimeter;

		NxReal					_angle;
		NxReal					_wheelWidth;
		NxReal					_maxSuspension;
		NxVec3					_maxPosition;

	};
}

#endif // __ZPV_SIMPLE_WHEEL_H__