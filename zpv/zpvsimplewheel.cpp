#include "stdafx.h"

namespace zpv
{

SimpleWheel::SimpleWheel( NxScene * s )
 : scene( s )
{ 
}

SimpleWheel::~SimpleWheel() 
{
	scene->releaseMaterial( *material );
}


void SimpleWheel::tick( bool handBrake,
					 NxReal motorTorque,
					 NxReal brakeTorque,
					 NxReal dt )
{
	if( getWheelFlag( NX_WF_ACCELERATED ) ) 
	{
		if( handBrake && getWheelFlag( NX_WF_AFFECTED_BY_HANDBRAKE ) ) 
		{
			// Handbrake, blocking!
		}
		else if( hasGroundContact() ) 
		{
			// Touching, force applies
			NxVec3 steeringDirection;
			getSteeringDirection( steeringDirection );
			steeringDirection.normalize();
			NxReal localTorque = motorTorque;
			NxReal wheelForce = localTorque / _radius;
			steeringDirection *= wheelForce;
			wheelCapsule->getActor().addForceAtPos( steeringDirection, 
				                                    contactInfo.contactPosition );
			if( contactInfo.otherActor->isDynamic() )
				contactInfo.otherActor->addForceAtPos( -steeringDirection,
				                                       contactInfo.contactPosition );
			} 
	}

	NxReal OneMinusBreakPedal = 1 - brakeTorque;
	if( handBrake && getWheelFlag( NX_WF_AFFECTED_BY_HANDBRAKE ) ) 
	{
		material->setDynamicFrictionV(1);
		material->setStaticFrictionV(4);
		material->setDynamicFriction(0.4f);
		material->setStaticFriction(1.0f);
	} 
	else 
	{
		NxReal newv  = OneMinusBreakPedal * _frictionToFront + brakeTorque;
		NxReal newv4 = OneMinusBreakPedal * _frictionToFront + brakeTorque * 4;
		material->setDynamicFrictionV( newv );
		material->setDynamicFriction( _frictionToSide );
		material->setStaticFrictionV( newv * 4 );
		material->setStaticFriction( 2 );
	}

	if( !hasGroundContact() )
		updateContactPosition();
	updateAngularVelocity( dt, handBrake );


	contactInfo.reset();
}

void SimpleWheel::getSteeringDirection(NxVec3& dir) 
{
	if( wheelFlags & ( NX_WF_STEERABLE_INPUT | NX_WF_STEERABLE_AUTO ) ) 
	{
		wheelCapsule->getGlobalOrientation().getColumn( 0, dir );
	} 
	else 
	{
		wheelCapsule->getActor().getGlobalOrientation().getColumn( 0, dir );
	}
}

void SimpleWheel::updateAngularVelocity( NxReal lastTimeStepSize, bool handbrake ) 
{
	if( ( wheelFlags & NX_WF_AFFECTED_BY_HANDBRAKE ) && handbrake ) 
	{
		_turnVelocity = 0;
	} 
	else if( contactInfo.isTouching() ) 
	{
		NxReal wheelPerimeter = NxTwoPi * _radius;
		NxReal newTurnVelocity = contactInfo.relativeVelocity / wheelPerimeter;
		_turnVelocity = newTurnVelocity;
		_turnAngle += _turnVelocity * lastTimeStepSize * NxTwoPi;
	} 
	else 
	{
		_turnVelocity *= 0.99f;
		_turnAngle += _turnVelocity;
	}
	while( _turnAngle >= NxTwoPi )
		_turnAngle -= NxTwoPi;
	while( _turnAngle < 0 )
		_turnAngle += NxTwoPi;
}

void SimpleWheel::drawWheel(NxReal approx, bool debug) const
{
	if(debug) 
	{
		glPointSize(5.0f);
		glEnableClientState(GL_VERTEX_ARRAY);
		NxVec3 vec1(getWheelPos());
		glColor4f(1,0,0,1);
		glVertexPointer(3, GL_FLOAT, 0, &vec1);
		glDrawArrays(GL_POINTS, 0, 1);

		NxVec3 vec2(contactInfo.contactPositionLocal);
		glColor4f(1,0,1,1);
		glVertexPointer(3, GL_FLOAT, 0, &vec2);
		glDrawArrays(GL_POINTS, 0, 1);

		NxVec3 wheelpos = contactInfo.contactPositionLocal + NxVec3(0.f, _radius, 0.f);
		glColor4f(1,1,1,1);
		glVertexPointer(3, GL_FLOAT, 0, &wheelpos);
		glDrawArrays(GL_POINTS, 0, 1);

		glDisableClientState(GL_VERTEX_ARRAY);
	}

#if !defined(__CELLOS_LV2__) && !defined(_XBOX)

	static GLUquadricObj *quadric = NULL;
	if(quadric == NULL) 
	{
		quadric = gluNewQuadric();
	}
	
	if(debug) 
	{
		gluQuadricDrawStyle(quadric, GLU_LINE);
	} 
	else 
	{
		gluQuadricDrawStyle(quadric, GLU_FILL);
	}

	NxVec3 wheelpos = (contactInfo.contactPositionLocal + NxVec3(0.f, _radius, 0.f));
	NxReal angle = NxMath::radToDeg(_turnAngle);
	
	glPushMatrix();
	
	if(debug) 
	{
		NxF32 red = getWheelFlag(NX_WF_ACCELERATED)?1.f:0.f;
		NxF32 green = contactInfo.isTouching()?1:0.5f;
		NxF32 blue = 0.5f;
		glColor4f(red, green, blue, 1.0f);
	} else {
		glColor4f(GL_COLOR_TIRE);
	}
	int slices = 20;
	GLfloat mat[16]; mat[3] = mat[7] = mat[11] = mat[12] = mat[13] = mat[14] = 0; mat[15] = 1;
	wheelCapsule->getLocalOrientation().getColumnMajorStride4(mat);
	
	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(3, GL_FLOAT, sizeof(NxVec3), &_maxPosition.x);
	glDrawArrays(GL_POINTS, 0, 1);
	glDisableClientState(GL_VERTEX_ARRAY);

	glTranslatef(wheelpos.x, wheelpos.y, wheelpos.z);
	glMultMatrixf(mat);
	glRotatef(angle, 0,0,1);
	glTranslatef(0,0, -_wheelWidth);
	gluCylinder(quadric, _radius, _radius, _wheelWidth*2, slices, 1);

	if(!debug) 
	{
		glTranslatef(0,0, 2*_wheelWidth);
		gluDisk(quadric, _radius/2.f, _radius, slices, 1);
		glColor4f(GL_COLOR_TIRE_INNER);
		gluDisk(quadric, 0, _radius/2.f, slices, 1);
		glTranslatef(0,0, -2*_wheelWidth);
		glRotatef(180,1,0,0);
		glColor4f(GL_COLOR_TIRE);
		gluDisk(quadric, _radius/2.f, _radius, slices, 1);
		glColor4f(GL_COLOR_TIRE_INNER);
		gluDisk(quadric, 0, _radius/2.f, slices, 1);
	}

	glPopMatrix();
#endif

}

void SimpleWheel::updateContactPosition() 
{
	contactInfo.contactPositionLocal = _maxPosition - NxVec3(0, _maxSuspension+_radius, 0);
}

void SimpleWheel::setAngle(NxReal angle) 
{
	_angle = angle;

	NxReal Cos, Sin;
	NxMath::sinCos(_angle, Sin, Cos);
	NxMat33 wheelOrientation = wheelCapsule->getLocalOrientation();
	wheelOrientation.setColumn(0,  NxVec3( Cos, 0, Sin ));
	wheelOrientation.setColumn(2,  NxVec3( Sin, 0,-Cos ));
	setWheelOrientation(wheelOrientation);
}



}