#ifndef __ZPV_VEHICLE_DESC_H__
#define __ZPV_VEHICLE_DESC_H__

#include "ZpvWheelDesc.h"
#include "ZpvVehicleMotorDesc.h"
#include "ZpvVehicleGearsDesc.h"
#include <NxArray.h>
#include <NxShapeDesc.h>

namespace zpv
{

	class VehicleDesc
	{
	public:
		NxArray<NxShapeDesc*>	carShapes;
		NxArray<WheelDesc*>	carWheels;

		NxArray<VehicleDesc*> children;

		VehicleMotorDesc*		_motorDesc;
		VehicleGearsDesc*		gearDesc;

		NxVec3					position;
		NxReal					mass;
		NxReal					motorForce;
		NxReal					transmissionEfficiency;
		NxReal					differentialRatio;

		NxVec3					steeringTurnPoint;
		NxVec3					steeringSteerPoint;
		NxReal					steeringMaxAngle;

		NxVec3					centerOfMass;

		NxReal					digitalSteeringDelta;

		NxReal					maxVelocity;
		NxReal					cameraDistance;

		void*					userData;

		NX_INLINE VehicleDesc();
		NX_INLINE void setToDefault();
		NX_INLINE bool isValid() const;
	};

	NX_INLINE VehicleDesc::VehicleDesc()	//constructor sets to default
	{
		setToDefault();
	}

	NX_INLINE void VehicleDesc::setToDefault()
	{
		userData = NULL;
		_motorDesc = NULL;
		gearDesc = NULL;
		transmissionEfficiency = 1.0f;
		differentialRatio = 1.0f;
		maxVelocity = 80;
		cameraDistance = 15.f;
		children.clear();
		carWheels.clear();
	}

	NX_INLINE bool VehicleDesc::isValid() const
	{
		for (NxU32 i = 0; i < carWheels.size(); i++) {
			if (!carWheels[i]->isValid())
				return false;
		}

		if (mass < 0)
			return false;

		return true;
	}
}

#endif // __ZPV_VEHICLE_DESC_H__
