#include "stdafx.h"

#include <cstdio>

#include <map>

#include <GL>

#include "ZpvVehicle.h"
#include "ZpvWheel.h"

#include <NxActorDesc.h>
//#include <NxAllocateable.h>
#include <NxBoxShape.h>
#include <NxConvexShape.h>
#include <NxConvexShapeDesc.h>
#include <NxConvexMesh.h>

#define TRAIL_FREQUENCY 0.025f

namespace zpv
{

	Vehicle::Vehicle() : m_SteeringWheelState(0), m_Scene(NULL), m_CarMaterial(NULL) 
	{ 
		memset(m_TrailBuffer, 0, sizeof(NxVec3) * NUM_TRAIL_POINTS);
		m_NextTrailSlot = 0;
		m_LastTrailTime = 0.0f;
	}

	Vehicle::~Vehicle()
	{
		if (m_CarMaterial)
			m_Scene->releaseMaterial(*m_CarMaterial);

		if (m_BodyActor)
			m_Scene->releaseActor(*m_BodyActor);
		for(NxU32 i = 0; i < m_Wheels.size(); i++)
		{
			if(m_Wheels[i])
			{
				delete m_Wheels[i];
				m_Wheels[i] = NULL;
			}
		}
	}

	Vehicle* Vehicle::_createVehicle(NxScene* scene, VehicleDesc* vehicleDesc)
	{
		//printf("Create Vehicle\n");

		if(vehicleDesc == NULL)
			return NULL;

		Vehicle* vehicle = new Vehicle;
		vehicle->userData = vehicleDesc->userData;

		if(!vehicleDesc->isValid())
		{
			printf("Vehicle Desc not valid!!\n");
			return NULL;
		}
		
		vehicle->m_Scene = scene;

		if(vehicle->m_CarMaterial == NULL)
		{
			NxMaterialDesc carMaterialDesc;
			carMaterialDesc.dynamicFriction = 0.4f;
			carMaterialDesc.staticFriction = 0.4f;
			carMaterialDesc.restitution = 0;
			carMaterialDesc.frictionCombineMode = NX_CM_MULTIPLY;
			vehicle->m_CarMaterial = scene->createMaterial(carMaterialDesc);
		}
		
		NxActorDesc actorDesc;
		for(NxU32 i = 0; i < vehicleDesc->carShapes.size(); i++)
		{
			actorDesc.shapes.pushBack(vehicleDesc->carShapes[i]);
			if (actorDesc.shapes[i]->materialIndex == 0)
				actorDesc.shapes[i]->materialIndex = vehicle->m_CarMaterial->getMaterialIndex();
		}

		NxBodyDesc bodyDesc;
		bodyDesc.mass = vehicleDesc->mass;
		bodyDesc.sleepEnergyThreshold = 0.05;
		actorDesc.body = &bodyDesc;
		actorDesc.globalPose.t = vehicleDesc->position;

		vehicle->m_BodyActor = scene->createActor(actorDesc);
		if(vehicle->m_BodyActor == NULL)
		{
			delete vehicle;
			return NULL;
		}
		vehicle->m_BodyActor->userData = vehicle;

		if(vehicleDesc->_motorDesc != NULL)
		{
			vehicle->m_VehicleMotor = VehicleMotor::createMotor(*vehicleDesc->_motorDesc);
			if(vehicle->m_VehicleMotor == NULL)
			{
				delete vehicle;
				return NULL;
			}
			vehicle->m_MotorForce = 0;
		} else {
			vehicle->m_VehicleMotor = NULL;
			vehicle->m_MotorForce = vehicleDesc->motorForce;
		}

		if(vehicleDesc->gearDesc != NULL)
		{
			vehicle->m_VehicleGears = VehicleGears::createVehicleGears(*vehicleDesc->gearDesc);
			if (vehicle->m_VehicleGears == NULL) {
				printf("Vehicle gear-creation failed\n");
				delete vehicle;
				return NULL;
			}
		} else {
			vehicle->m_VehicleGears = NULL;
		}

		for(NxU32 i = 0; i < vehicleDesc->carWheels.size(); i++)
		{
			Wheel* wheel = Wheel::createWheel(vehicle->m_BodyActor, vehicleDesc->carWheels[i]);
			if(wheel)
			{
				vehicle->m_Wheels.pushBack(wheel);
			} else {
				delete vehicle;
				return NULL;
			}
		}

		vehicle->m_DigitalSteeringDelta		= vehicleDesc->digitalSteeringDelta;
		vehicle->m_SteeringSteerPoint		= vehicleDesc->steeringSteerPoint;
		vehicle->m_SteeringTurnPoint			= vehicleDesc->steeringTurnPoint;
		vehicle->m_SteeringMaxAngleRad		= NxMath::degToRad(vehicleDesc->steeringMaxAngle);
		vehicle->m_TransmissionEfficiency	= vehicleDesc->transmissionEfficiency;
		vehicle->m_DifferentialRatio			= vehicleDesc->differentialRatio;
		vehicle->m_MaxVelocity				= vehicleDesc->maxVelocity;
		vehicle->m_CameraDistance			= vehicleDesc->cameraDistance;
		vehicle->m_BodyActor->setCMassOffsetLocalPosition(vehicleDesc->centerOfMass);

		//don't go to sleep.
		//vehicle->m_BodyActor->wakeUp(1e10);
		
		vehicle->control(0, true, 0, true, false);
		return vehicle;
	}

	Vehicle* Vehicle::createVehicle(NxScene* scene, VehicleDesc* vehicleDesc)
	{
		if (vehicleDesc == NULL)
			return NULL;
		Vehicle* vehicle = Vehicle::_createVehicle(scene, vehicleDesc);
		VehicleManager::AddVehicle(vehicle);
		if (VehicleManager::getActiveVehicleNumber() != -1 || VehicleManager::getNumberOfVehicles() == 1)
			VehicleManager::setActiveVehicle(VehicleManager::getNumberOfVehicles()-1);

		for(NxU32 i = 0; i < vehicleDesc->children.size(); i++)
		{
			Vehicle* child = Vehicle::_createVehicle(scene, vehicleDesc->children[i]);
			if(child != NULL)
			{
				vehicle->addChild(child);
				VehicleManager::AddChildVehicle(child);
			}
			else
			{
				fprintf(stderr, "Warning, child %d could not be created\n", i);
			}
		}

		return vehicle;
	}

	void Vehicle::handleContactPair(NxContactPair& pair, int carIndex)
	{
		NxContactStreamIterator i(pair.stream);
		
		while(i.goNextPair())
		{
			NxShape * s = i.getShape(carIndex);
			
			while(i.goNextPatch())
			{
				const NxVec3& contactNormal = i.getPatchNormal();
				
				while(i.goNextPoint())
				{
					//user can also call getPoint() and getSeparation() here
		
					const NxVec3& contactPoint = i.getPoint();

					//add forces:

					//assuming front wheel drive we need to apply a force at the wheels.
					if (s->is(NX_SHAPE_CAPSULE) && s->userData != NULL)
					{
						//assuming only the wheels of the car are capsules, otherwise we need more checks.
						//this branch can't be pulled out of loops because we have to do a full iteration through the stream
					
						NxQuat local2global = s->getActor().getGlobalOrientationQuat();
						Wheel* w = (Wheel*)s->userData;
						if (!w->getWheelFlag(NX_WF_USE_WHEELSHAPE))
						{
							SimpleWheel * wheel = static_cast<SimpleWheel*>(w);
							wheel->contactInfo.otherActor = pair.actors[1-carIndex];
							wheel->contactInfo.contactPosition = contactPoint;
							
							wheel->contactInfo.contactPositionLocal = contactPoint;
							wheel->contactInfo.contactPositionLocal -= m_BodyActor->getGlobalPosition();
							local2global.inverseRotate(wheel->contactInfo.contactPositionLocal);
							
							wheel->contactInfo.contactNormal = contactNormal;
							if (wheel->contactInfo.otherActor->isDynamic()) 
							{
								NxVec3 globalV = s->getActor().getLocalPointVelocity(wheel->getWheelPos());
								globalV -= wheel->contactInfo.otherActor->getLinearVelocity();
								local2global.inverseRotate(globalV);
								wheel->contactInfo.relativeVelocity = globalV.x;
								//printf("%2.3f (%2.3f %2.3f %2.3f)\n", wheel->contactInfo.relativeVelocity,
								//	globalV.x, globalV.y, globalV.z);
							} 
							else 
							{
								NxVec3 vel = s->getActor().getLocalPointVelocity(wheel->getWheelPos());
								local2global.inverseRotate(vel);
								wheel->contactInfo.relativeVelocity = vel.x;
								wheel->contactInfo.relativeVelocitySide = vel.z;
							}
							NX_ASSERT(wheel->hasGroundContact());
							//printf(" Wheel %x is touching\n", wheel);
						}
					}
				}
			}		
		}
		//printf("----\n");
	}

	void Vehicle::updateVehicle(NxReal lastTimeStepSize)
	{
		//printf("updating %x\n", this);
		
		NxReal distanceSteeringAxisCarTurnAxis = m_SteeringSteerPoint.x  - m_SteeringTurnPoint.x;
		NX_ASSERT(m_SteeringSteerPoint.z == m_SteeringTurnPoint.z);
		NxReal distance2 = 0;
		if (NxMath::abs(m_SteeringWheelState) > 0.01f)
			distance2 = distanceSteeringAxisCarTurnAxis / NxMath::tan(m_SteeringWheelState * m_SteeringMaxAngleRad);

		//printf("d1 = %2.3f, d2 = %2.3f, a1 = %2.3f, a2 = %2.3f\n",
		//	distanceSteeringAxisCarTurnAxis, distance2,
		//	m_SteeringWheelState, m_SteeringWheelState * m_SteeringMaxAngleRad);


		m_LastTrailTime += lastTimeStepSize;

		if(m_LastTrailTime > TRAIL_FREQUENCY)
		{
			m_LastTrailTime = 0.0f;
		}

		int nbTouching = 0;
		int nbNotTouching = 0;
		int nbHandBrake = 0;
		for(NxU32 i = 0; i < m_Wheels.size(); i++)
		{
			Wheel* wheel = m_Wheels[i];

			if (m_LastTrailTime  == 0.0f)
			{
				if(m_Wheels[i]->hasGroundContact())
				{
					if (++m_NextTrailSlot >= NUM_TRAIL_POINTS)
						m_NextTrailSlot = 0;
					m_TrailBuffer[m_NextTrailSlot] = m_BodyActor->getGlobalPose() * m_Wheels[i]->getGroundContactPos();
				}
			}

			if(wheel->getWheelFlag(NX_WF_STEERABLE_INPUT))
			{
				if(distance2 != 0)
				{
					NxReal xPos = wheel->getWheelPos().x;
					NxReal zPos = wheel->getWheelPos().z;
					NxReal dz = -zPos + distance2;
					NxReal dx = xPos - m_SteeringTurnPoint.x;
					wheel->setAngle(NxMath::atan(dx/dz));
				} else {
					wheel->setAngle(0.f);
				}
				//printf("%2.3f\n", wheel->getAngle());

			} else if(wheel->getWheelFlag(NX_WF_STEERABLE_AUTO))
				{
				NxVec3 localVelocity = m_BodyActor->getLocalPointVelocity(wheel->getWheelPos());
				NxQuat local2Global = m_BodyActor->getGlobalOrientationQuat();
				local2Global.inverseRotate(localVelocity);
	//			printf("%2.3f %2.3f %2.3f\n", wheel->getWheelPos().x,wheel->getWheelPos().y,wheel->getWheelPos().z);
				localVelocity.y = 0;
				if(localVelocity.magnitudeSquared() < 0.01f)
				{
					wheel->setAngle(0.0f);
				} else {
					localVelocity.normalize();
	//				printf("localVelocity: %2.3f %2.3f\n", localVelocity.x, localVelocity.z);
					if(localVelocity.x < 0)
						localVelocity = -localVelocity;
					NxReal angle = NxMath::clamp((NxReal)atan(localVelocity.z / localVelocity.x), 0.3f, -0.3f);
					wheel->setAngle(angle);
				}
			}

			// now the acceleration part
			if(!wheel->getWheelFlag(NX_WF_ACCELERATED))
				continue;

			if(m_HandBrake && wheel->getWheelFlag(NX_WF_AFFECTED_BY_HANDBRAKE))
			{
				nbHandBrake++;
			} else {
				if (!wheel->hasGroundContact())
				{
					nbNotTouching++;
				} else {
					nbTouching++;
				}
			}
		}
		
		NxReal motorTorque = 0.0f; 
		if(nbTouching && NxMath::abs(m_AccelerationPedal) > 0.01f) 
		{
			NxReal axisTorque = computeAxisTorque();
			NxReal wheelTorque = axisTorque / (NxReal)(m_Wheels.size() - nbHandBrake);
			NxReal wheelTorqueNotTouching = nbNotTouching>0?wheelTorque*(NxMath::pow(0.5f, (NxReal)nbNotTouching)):0;
			NxReal wheelTorqueTouching = wheelTorque - wheelTorqueNotTouching;
			motorTorque = wheelTorqueTouching / (NxReal)nbTouching; 
		} else {
			updateRpms();
		}
	//printf("wt: %f %f\n", motorTorque, m_BrakePedal);
		for(NxU32 i = 0; i < m_Wheels.size(); i++) 
		{
			Wheel* wheel = m_Wheels[i];
			wheel->tick(m_HandBrake, motorTorque, m_BrakePedal, lastTimeStepSize);
		}

		//printf("---\n");
	}

	//finds the actor that is touched by most wheels.
	void Vehicle::computeMostTouchedActor()
	{
		std::map<NxActor*, int> actors;
		typedef std::map<NxActor*, int> Map;
		for(NxU32 i = 0; i < m_Wheels.size(); i++)
		{
			NxActor* curActor = m_Wheels[i]->getTouchedActor();
			Map::iterator it = actors.find(curActor);
			if (it == actors.end())
			{
				actors[curActor] = 1;
			} else {
				it->second++;
			}
		}

		int count = 0;
		m_MostTouchedActor = NULL;
		for(Map::iterator it = actors.begin(); it != actors.end(); ++it)
		{
			if(it->second > count)
			{
				count = it->second;
				m_MostTouchedActor = it->first;
			}
		}
	}

	void Vehicle::controlSteering(NxReal steering, bool analogSteering)
	{
		if(analogSteering)
		{
			m_SteeringWheelState = steering;
		} else if (NxMath::abs(steering) > 0.0001f) {
			m_SteeringWheelState += NxMath::sign(steering) * m_DigitalSteeringDelta;
		} else if (NxMath::abs(m_SteeringWheelState) > 0.0001f) {
			m_SteeringWheelState -= NxMath::sign(m_SteeringWheelState) * m_DigitalSteeringDelta;
		}
		m_SteeringWheelState = NxMath::clamp(m_SteeringWheelState, 1.f, -1.f);
		//printf("SteeringWheelState: %2.3f\n", m_SteeringWheelState);
	}

	void Vehicle::computeLocalVelocity()
	{
		computeMostTouchedActor();
		NxVec3 relativeVelocity;
		if (m_MostTouchedActor == NULL || !m_MostTouchedActor->isDynamic())
		{
			relativeVelocity = m_BodyActor->getLinearVelocity();
		} else {
			relativeVelocity = m_BodyActor->getLinearVelocity() - m_MostTouchedActor->getLinearVelocity();
		}
		NxQuat rotation = m_BodyActor->getGlobalOrientationQuat();
		NxQuat global2Local;
		m_LocalVelocity = relativeVelocity;
		rotation.inverseRotate(m_LocalVelocity);
		//printf("Velocity: %2.3f %2.3f %2.3f\n", m_LocalVelocity.x, m_LocalVelocity.y, m_LocalVelocity.z);
	}

	void Vehicle::controlAcceleration(NxReal acceleration, bool analogAcceleration)
	{
		if(NxMath::abs(acceleration) < 0.001f)
			m_ReleaseBraking = true;
		if(!m_Braking)
		{
			m_AccelerationPedal = NxMath::clamp(acceleration, 1.f, -1.f);
			m_BrakePedalChanged = m_BrakePedal == 0;
			m_BrakePedal = 0;
		} else {
			m_AccelerationPedal = 0;
			NxReal newv = NxMath::clamp(NxMath::abs(acceleration), 1.f, 0.f);
			m_BrakePedalChanged = m_BrakePedal == newv;
			m_BrakePedal = newv;
		}
		//printf("Acceleration: %2.3f, Braking %2.3f\n", m_AccelerationPedal, m_BrakePedal);
	}

	void Vehicle::control(NxReal steering, bool analogSteering, NxReal acceleration, bool analogAcceleration, bool handBrake)
	{
		if (steering != 0 || acceleration != 0 || handBrake)
			m_BodyActor->wakeUp(0.05);

		controlSteering(steering, analogSteering);
		computeLocalVelocity();
		if (!m_Braking || m_ReleaseBraking)
		{
			m_Braking = m_LocalVelocity.x * acceleration < (-0.1f /* NxMath::sign(-acceleration)*/);
			m_ReleaseBraking = false;
		}
		//printf("Braking: %s, Handbrake: %s\n", m_Braking?"true":"false", handBrake?"true":"false");
		if(m_HandBrake != handBrake)
		{
			m_HandBrake = handBrake;
			m_BrakePedalChanged;
		}
		controlAcceleration(acceleration, analogAcceleration);


	}

	void Vehicle::draw(bool debug) 
	{
		glPushMatrix();

		float glmat[16];
		m_BodyActor->getGlobalPose().getColumnMajor44(glmat);
		glMultMatrixf(glmat);

		if(debug) glDisable(GL_LIGHTING);

		for(unsigned int i=0;i<m_Wheels.size(); i++) 
		{
			m_Wheels[i]->drawWheel(5, debug);
		}

		if(debug) glEnable(GL_LIGHTING);

		glPopMatrix();

		glDisable(GL_LIGHTING);
		glEnable(GL_BLEND);
		glPointSize(10.0f);

		const float alphaStart = 0.3f;
		float alpha = alphaStart;

		static NxVec3 pBufferTrailPos[NUM_TRAIL_POINTS];
		static float pBufferTrailColor[NUM_TRAIL_POINTS*4];
		/*if(pBufferTrailPos == NULL)
		{
			pBufferTrailPos = new NxVec3[NUM_TRAIL_POINTS];
			pBufferTrailColor = new float[NUM_TRAIL_POINTS*4];
		}*/

		for(int x = NUM_TRAIL_POINTS-1; x >= 0; x--)
		{
			alpha-=alphaStart/NUM_TRAIL_POINTS;
			pBufferTrailColor[x*4+0] = 0.1f;
			pBufferTrailColor[x*4+1] = 0.1f;
			pBufferTrailColor[x*4+2] = 0.1f;
			pBufferTrailColor[x*4+3] = alpha;
			pBufferTrailPos[x]=m_TrailBuffer[(m_NextTrailSlot+x)%NUM_TRAIL_POINTS];
		}

		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_COLOR_ARRAY);
		glVertexPointer(3, GL_FLOAT, sizeof(NxVec3), pBufferTrailPos);
		glColorPointer(4, GL_FLOAT, 0, pBufferTrailColor);
		glDrawArrays(GL_POINTS, 0, NUM_TRAIL_POINTS);
		glDisableClientState(GL_VERTEX_ARRAY);
		glDisableClientState(GL_COLOR_ARRAY);

		glDisable(GL_BLEND);
		glEnable(GL_LIGHTING);

	}

	NxReal Vehicle::computeAxisTorque()
	{
		if(m_VehicleMotor != NULL)
		{
			NxReal rpm = computeRpmFromWheels();
			NxReal motorRpm = computeMotorRpm(rpm);
			m_VehicleMotor->setRpm(motorRpm);
			NxReal torque = m_AccelerationPedal * m_VehicleMotor->getTorque();
			NxReal v = m_BodyActor->getLinearVelocity().magnitude();
			//printf("v: %2.3f m/s (%2.3f km/h)\n", v, v*3.6f);
			//printf("rpm %2.3f, motorrpm %2.3f, torque %2.3f, realtorque %2.3f\n",
			//	rpm, motorRpm, torque, torque*getGearRatio()*m_DifferentialRatio*m_TransmissionEfficiency);
			return torque * getGearRatio() * m_DifferentialRatio * m_TransmissionEfficiency;
		} else {
			computeRpmFromWheels();
			return m_AccelerationPedal * m_MotorForce;
		}
	}


	NxReal Vehicle::computeRpmFromWheels()
	{
		NxReal wheelRpms = 0;
		NxI32 nbWheels = 0;
		for(NxU32 i = 0; i < m_Wheels.size(); i++)
		{
			Wheel* wheel = m_Wheels[i];
			if (wheel->getWheelFlag(NX_WF_ACCELERATED))
			{
				nbWheels++;
				wheelRpms += wheel->getRpm();
			}
		}
		return wheelRpms / (NxReal)nbWheels;
	}

	NxReal Vehicle::computeMotorRpm(NxReal rpm)
	{
		NxReal temp = getGearRatio() * m_DifferentialRatio;
		NxReal motorRpm = rpm * temp;
		if(m_VehicleMotor)
		{
			NxI32 change;
			if(m_VehicleGears && (change = m_VehicleMotor->changeGears(m_VehicleGears, 0.2f)))
			{
				if(change == 1)
				{
					gearUp();
				} else {
					NX_ASSERT(change == -1);
					gearDown();
				}
			}
			temp = getGearRatio() * m_DifferentialRatio;
			motorRpm = NxMath::max(rpm * temp, m_VehicleMotor->getMinRpm());
		}
		return motorRpm;
	}

	NxReal Vehicle::getGearRatio()
	{
		if(m_VehicleGears == NULL)
		{
			return 1;
		} else {
			return m_VehicleGears->getCurrentRatio();
		}
	}

	void Vehicle::gearUp()
	{
		if (m_VehicleGears)
		{
			printf("Changing gear from %d to", m_VehicleGears->getGear());
			m_VehicleGears->gearUp();
			printf(" %d\n", m_VehicleGears->getGear());
		} else {
			printf("gearUp not supported if no gears available\n");
		}
	}
	void Vehicle::gearDown()
	{
		if(m_VehicleGears)
		{
			printf("Changing gear from %d to", m_VehicleGears->getGear());
			m_VehicleGears->gearDown();
			printf(" %d\n", m_VehicleGears->getGear());
		} else {
			printf("gearDown not supported if no gears available\n");
		}
	}

	void Vehicle::applyRandomForce()
	{
		NxVec3 pos(NxMath::rand(-4.f,4.f),NxMath::rand(-4.f,4.f),NxMath::rand(-4.f,4.f));
		NxReal force = NxMath::rand(m_BodyActor->getMass()*0.5f, m_BodyActor->getMass() * 2.f);
		m_BodyActor->addForceAtLocalPos(NxVec3(0, force*100.f, 0), pos);
	}

	void Vehicle::standUp()
	{
		NxVec3 pos = getActor()->getGlobalPosition() + NxVec3(0,2,0);
		NxQuat rot = getActor()->getGlobalOrientationQuat();
		NxVec3 front(1,0,0);
		rot.rotate(front);
		front.y = 0;
		front.normalize();

		NxReal dotproduct  = front.x;

		NxReal angle = NxMath::sign(-front.z) * NxMath::acos(dotproduct);

		rot.fromAngleAxis(NxMath::radToDeg(angle), NxVec3(0,1,0));
		getActor()->setGlobalPosition(pos);
		getActor()->setGlobalOrientationQuat(rot);
		getActor()->setLinearVelocity(NxVec3(0,0,0));
		getActor()->setAngularVelocity(NxVec3(0,0,0));
	}

	void Vehicle::updateRpms()
	{
		NxReal rpm = computeRpmFromWheels();
		if(m_VehicleMotor != NULL)
		{
			NxReal motorRpm = computeMotorRpm(rpm);
			m_VehicleMotor->setRpm(motorRpm);
		}
	}

}