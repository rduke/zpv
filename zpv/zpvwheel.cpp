#include "stdafx.h"

namespace zpv
{

	Wheel* Wheel::createWheel( NxActor* actor, WheelDesc* wheelDesc ) 
	{
		if( wheelDesc->wheelFlags & NX_WF_USE_WHEELSHAPE )
		{
			return new ShapeWheel( actor, wheelDesc );
		}
		
		SimpleWheel* wheel = new SimpleWheel( &actor->getScene() );

		if( wheelDesc->wheelApproximation > 0 ) 
		{
			NxArray< NxVec3 > points;
			NxVec3 center = wheelDesc->position;

			NxVec3 frontAxis( 1, 0, 0 );
			NxVec3 downAxis( 0, -1, 0 );
			NxVec3 wheelAxis( 0, 0, -1 );

			frontAxis *= wheelDesc->wheelRadius;

			downAxis *= wheelDesc->wheelRadius;

			wheelAxis *= wheelDesc->wheelWidth;
			NxReal step;
			if( wheelDesc->wheelFlags & NX_WF_BUILD_LOWER_HALF ) 
			{
				if( ( wheelDesc->wheelApproximation & 0x1 ) == 0 )
					wheelDesc->wheelApproximation++;
				step = ( NxReal )( NxTwoPi ) / ( NxReal )( wheelDesc->wheelApproximation * 2 );
			}
			else 
			{
				step = ( NxReal )( NxTwoPi ) / ( NxReal )( wheelDesc->wheelApproximation );
			}
			for(int i = 0; i < wheelDesc->wheelApproximation; i++) 
			{
				NxReal iReal;
				if( wheelDesc->wheelFlags & NX_WF_BUILD_LOWER_HALF ) 
				{
					iReal = ( i > ( wheelDesc->wheelApproximation >> 1 ) ) ? 
								  ( NxReal )( i + wheelDesc->wheelApproximation ) :
								  ( NxReal )i;
				} 
				else 
				{
					iReal = (NxReal)i;
				}
				NxReal Sin, Cos;
				NxMath::sinCos( step * iReal, Sin, Cos );
				NxVec3 insPoint = ( downAxis * -Cos ) + ( frontAxis * Sin );
				points.pushBack( insPoint + wheelAxis );
				points.pushBack( insPoint - wheelAxis );
			}

			NxConvexMeshDesc convexDesc;
			convexDesc.numVertices			= points.size();
			convexDesc.pointStrideBytes		= sizeof( NxVec3 );
			convexDesc.points				= &points[ 0 ].x;
			convexDesc.flags				= NX_CF_COMPUTE_CONVEX;

			// Cooking from memory
			MemoryWriteBuffer buf;
			if( CookingManager::getSingletonRef().cookConvexMesh( convexDesc, buf ) ) 
			{
				NxConvexShapeDesc convexShapeDesc;
				convexShapeDesc.meshData = actor->getScene().getPhysicsSDK().createConvexMesh( MemoryReadBuffer( buf.data ) );
				convexShapeDesc.localPose.t = center;
				convexShapeDesc.localPose.M.setColumn( 0, NxVec3( 1,  0,  0 ) );
				convexShapeDesc.localPose.M.setColumn( 1, NxVec3( 0, -1,  0 ) );
				convexShapeDesc.localPose.M.setColumn( 2, NxVec3( 0,  0, -1 ) );
				if( convexShapeDesc.meshData != NULL ) 
				{
					int shapeNumber = actor->getNbShapes();
					wheel->wheelConvex = actor->createShape( convexShapeDesc )->isConvexMesh();
					wheel->wheelConvex->userData = wheel;
				}
			}
			else 
			{
				delete wheel;
				return NULL;
			}
		}
		else 
		{
			wheel->wheelConvex = NULL;
		}

		NxVec3 frontAxis( 1,  0,  0 );
		NxVec3 downAxis ( 0, -1,  0 );
		NxVec3 wheelAxis( 0,  0, -1 );

		// This assures a height of 1 for every capsule
		NxReal heightModifier = ( wheelDesc->wheelSuspension + wheelDesc->wheelRadius )
								 / wheelDesc->wheelSuspension;

		NxSpringDesc wheelSpring;
		wheelSpring.spring					= wheelDesc->springRestitution * heightModifier;
		wheelSpring.damper					= wheelDesc->springDamping * heightModifier;
		wheelSpring.targetValue				= wheelDesc->springBias * heightModifier;

		NxMaterialDesc materialDesc;
		materialDesc.restitution			= 0.0f;
		materialDesc.dynamicFriction		= wheelDesc->frictionToSide;
		materialDesc.staticFriction			= 2.0f;
		materialDesc.staticFrictionV		= wheelDesc->frictionToFront * 4;
		materialDesc.dynamicFrictionV		= wheelDesc->frictionToFront;
		materialDesc.dirOfAnisotropy		= frontAxis;
		materialDesc.frictionCombineMode	= NX_CM_MULTIPLY;
		materialDesc.flags					|=  NX_MF_ANISOTROPIC;

		wheel->_frictionToFront = wheelDesc->frictionToFront;
		wheel->_frictionToSide = wheelDesc->frictionToSide;

		wheel->material = actor->getScene().createMaterial( materialDesc );

		NxCapsuleShapeDesc capsuleShape;
		capsuleShape.radius = 0.1f;
		capsuleShape.height = wheelDesc->wheelSuspension + wheelDesc->wheelRadius;
		capsuleShape.flags = NX_SWEPT_SHAPE;

		capsuleShape.localPose.M.setColumn( 0, frontAxis );
		capsuleShape.localPose.M.setColumn( 1, downAxis  );
		capsuleShape.localPose.M.setColumn( 2, wheelAxis );
		if(wheelDesc->wheelSuspension >= 1) 
		{
			capsuleShape.localPose.t = wheelDesc->position + downAxis * ( wheelDesc->wheelRadius );
		}
		else 
		{
			capsuleShape.localPose.t = wheelDesc->position + 
									   downAxis * 
										   ( ( wheelDesc->wheelRadius + wheelDesc->wheelSuspension ) * 0.5f );
		}
		capsuleShape.materialIndex = wheel->material->getMaterialIndex();
		capsuleShape.userData = wheelDesc->userData;

		int shapeNumber = actor->getNbShapes();
		wheel->wheelCapsule = actor->createShape(capsuleShape)->isCapsule();

		if(wheel->wheelCapsule == NULL) 
		{
			delete wheel;
			return NULL;
		}
		wheel->wheelCapsule->userData = wheel;
		wheel->userData = wheelDesc->userData;
		wheel->wheelCapsule->userData = wheel;
		wheel->wheelFlags = (wheelDesc->wheelFlags & NX_WF_ALL_WHEEL_FLAGS);
		wheel->_turnAngle = 0;
		wheel->_turnVelocity = 0;
		wheel->_radius = wheelDesc->wheelRadius;
		wheel->_perimeter = wheel->_radius * NxTwoPi;
		wheel->_maxSuspension = wheelDesc->wheelSuspension;
		wheel->_wheelWidth = wheelDesc->wheelWidth;
		wheel->_maxPosition = wheelDesc->position;
		return wheel;
	}

}