#ifndef __ZPV_PHYSX_MANAGER_H__
#define __ZPV_PHYSX_MANAGER_H__

#include "zpvSingleton.h"
#include "zpvutilities.h"

#define GL_COLOR_WHITE		1.0f,1.0f,1.0f,1.0f
#define GL_COLOR_RED		1.0f,0.0f,0.0f,1.0f
#define GL_COLOR_GREEN		0.0f,1.0f,0.0f,1.0f
#define GL_COLOR_BLUE		0.0f,0.0f,1.0f,1.0f
#define GL_COLOR_LIGHT_BLUE 0.5f,0.5f,1.0f,1.0f
#define GL_COLOR_LIGHT_GREY 0.7f,0.7f,0.7f,1.0f
#define GL_COLOR_BLACK		0.0f,0.0f,0.0f,1.0f


namespace zpv
{

	enum CreationModes
	{
		MODE_NONE,
		MODE_CAR,
		MODE_TRUCK
	};

	class PhysXManager
		: public Singleton< PhysXManager >
	{
	public:
		NxPhysicsSDK* getSDK();
		NxScene*      getDefaultScene();
		void          releaseSDK();
		NxMaterialIndex* getTerrainMaterials() const { return m_TerrainMaterials; }

	protected:
		friend class Singleton< PhysXManager >;
		explicit PhysXManager();

	private:
		bool initSDK();
		bool initCooking();
		void initTerrain();
		static float trand();
		void          releaseDefaultScene();
		NxReal chooseTrigMaterial(int faceIndex, NxReal lastSteepness);
		NxActor* createTerrain( NxScene* _scene );

	private:
		NxPhysicsSDK*    m_SDK;
		NxScene*         m_Scene;
		ErrorStream	     m_ErrorStream;
		NxVec3*          m_TerrainVerts;
		NxVec3*          m_TerrainNormals;
		int*           m_TerrainFaces;
		NxMaterialIndex* m_TerrainMaterials;
		NxMaterialIndex  materialIce, materialRock, materialMud, materialGrass, materialDefault;
	};


}

#endif // __ZPV_PHYSX_MANAGER_H__