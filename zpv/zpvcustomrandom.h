#ifndef __ZPV_CUSTOM_RANDOM_H__
#define __ZPV_CUSTOM_RANDOM_H__

namespace zpv
{

	class CustomRandom
	{
		unsigned long next;

	public:

		static const unsigned int rand_max = 32767;

		static float trand()
		{
			static CustomRandom random( 3 );
			return ( (float)(random.rand() ) / ( (float)CustomRandom::rand_max+1) ) * (2.0f) -1.0f;
		}

		CustomRandom( unsigned seed = 1 )
		{
			next = 0;
			srand( seed );
		}

		int rand( void )  /* RAND_MAX assumed to be 32767. */
		{
			next = next * 1103515245 + 12345;
			return( ( unsigned )( next / 65536 ) % 32768 );
		}

		void srand( unsigned seed )
		{
			next = seed;
		}
	};

}

#endif // __ZPV_CUSTOM_RANDOM_H__