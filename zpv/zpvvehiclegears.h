#ifndef __ZPV_VEHICLE_GEARS_H__
#define __ZPV_VEHICLE_GEARS_H__

#include "ZpvVehicleGearsDesc.h"
#include <NxArray.h>

namespace zpv
{

	class VehicleGears 
	{
	
		int									_nbForwardGears;
	
		float								_forwardGearRatios[NX_VEHICLE_MAX_NB_GEARS];

		float								_backwardGearRatio;

		int									_curGear;


	public:
		VehicleGears(): _curGear(1) { }

		static VehicleGears* createVehicleGears(const VehicleGearsDesc& gearDesc);
		
		float					getCurrentRatio() const;
		float					getRatio(int gear) const;
		int					getGear() const { return _curGear; }
		int					getMaxGear() const { return _nbForwardGears; }
		void					gearUp() { _curGear = NxMath::min(_curGear+1, (int)_nbForwardGears); }
		void					gearDown() { _curGear = NxMath::max(_curGear-1, -1); }
	};

	inline VehicleGears* VehicleGears::createVehicleGears(const VehicleGearsDesc& gearDesc)
	{
		if (!gearDesc.isValid())
			return NULL;
		VehicleGears *gears = new VehicleGears();
		int nbForwardGears = gears->_nbForwardGears = gearDesc.nbForwardGears;
		memcpy(gears->_forwardGearRatios, gearDesc.forwardGearRatios, sizeof(float) * nbForwardGears);

		gears->_curGear = 1;


		gears->_backwardGearRatio = gearDesc.backwardGearRatio;

		return gears;
	}

	inline float VehicleGears::getCurrentRatio() const
	{
		return getRatio(_curGear);
	}

	inline float VehicleGears::getRatio(int gear) const
	{
		if (gear > 0)
			return _forwardGearRatios[gear-1];
		if (gear == -1)
			return _backwardGearRatio;
		return 0;
	}
}

#endif // __ZPV_VEHICLE_GEARS_H__
