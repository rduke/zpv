#ifndef __ZPV_MATERIAL_FACOTRY_H__
#define __ZPV_MATERIAL_FACOTRY_H__

#include <NxPhysics.h>

namespace zpv
{

	class MaterialFactory
	{
	public:
		MaterialFactory( NxScene* _scene = PhysXManager::getSingletonRef().getDefaultScene() );
		NxMaterialIndex getRock()    const { return m_Rock; }
		NxMaterialIndex getIce()     const { return m_Ice; }
		NxMaterialIndex getMud()     const { return m_Mud; }
		NxMaterialIndex getGrass()   const { return m_Grass; }
		NxMaterialIndex getDefault() const { return m_Scene->getMaterialFromIndex( 0 )->getMaterialIndex(); }

	private:
		NxScene*        m_Scene;
		NxMaterialIndex m_Rock;
		NxMaterialIndex m_Ice;
		NxMaterialIndex m_Mud;
		NxMaterialIndex m_Grass;
	};

}

#endif // __ZPV_MATERIAL_FACOTRY_H__