#include "stdafx.h"
#include <boost/foreach.hpp>

namespace zpv
{

	MaterialFactory* MaterialManager::factoryForScene( NxScene* _scene )
	{
		MaterialFactory* factory = m_Factories[ _scene ];
		if( !factory )
		{
			factory = new MaterialFactory( _scene );
            m_Factories[ _scene ] = factory;
		}
		return factory;
	}

	MaterialManager::~MaterialManager()
	{
		BOOST_FOREACH( MaterialFactoryMap::value_type& pair, m_Factories )
		{
			delete pair.second;
		}
	}

}