#ifndef __ZPV_VEHICLE_FACTORY_H__
#define __ZPV_VEHICLE_FACTORY_H__

#include "zpvSingleton.h"
#include "NxPhysics.h"
#include "zpvVehicleGearsDesc.h"
#include "zpvVehicleMotorDesc.h"
#include "zpvVehicle.h"
#include "zpvWheelDesc.h"

namespace zpv
{

	class VehicleFactory
		: public Singleton< VehicleFactory >
	{
	public:
		void createCarWithDesc( const NxVec3& _pos,
			                    bool          _frontWheelDrive,
								bool          _backWheelDrive,
								bool          _corvetteMotor,
								bool          _monsterTruck,
								bool          _oldStyle,
								NxPhysicsSDK* _physicsSDK );

	void createTestCarWithDesc( const NxVec3& pos,
								bool frontWheelDrive,
								bool backWheelDrive,
								bool corvette,
								bool monsterTruck,
								bool oldStyle,
								NxPhysicsSDK* physicsSDK );

		void createCart( const NxVec3& _pos,
			             bool          _frontWheelDrive,
						 bool          _backWheelDrive,
						 bool          _oldStyle );

		VehicleDesc* createTruckPullerDesc( const NxVec3& _pos,
			                                int         _nbGears,
											bool          _oldStyle );

		VehicleDesc* createTruckTrailer1( const NxVec3& _pos,
			                              NxReal        _length,
										  bool          _oldStyle );

		VehicleDesc* createFullTruckDesc( const NxVec3& _pos,
			                              NxReal        _length,
										  int         _nbGears,
										  bool          _has4Axes,
										  bool          _oldStyle );

		VehicleDesc* createTwoAxisTrailer( const NxVec3& _pos,
			                               NxReal        _length,
										   bool          _oldStyle );

		Vehicle* createTruckPuller( const NxVec3& _pos,
			                        int         _nbGears,
									bool          _oldStyle );

		Vehicle* createFullTruck( const NxVec3& _pos,
			                      int         _nbGears,
								  bool          _has4Axes,
								  bool          _oldStyle );

		Vehicle* createTruckWithTrailer1( const NxVec3& _pos,
			                              int         _nbGears,
										  bool          _oldStyle );

		Vehicle* createFullTruckWithTrailer2( const NxVec3& _pos,
			                                  int         _nbGears,
											  bool          _oldStyle );
	};

}

#endif // __ZPV_VEHICLE_FACTORY_H__