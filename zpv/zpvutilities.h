
#ifndef __ZPV_UTILITIES_H__
#define __ZPV_UTILITIES_H__

#if defined(WIN32)
#include <direct.h>
#include <shlwapi.h>
#endif

#if defined(__CELLOS_LV2__)
#define __forceinline inline __attribute__((always_inline))
#elif defined(LINUX)
#define __forceinline inline __attribute__((always_inline))
#endif

#include <Nx.h>

namespace zpv
{

	const char* getNxSDKCreateError( const NxSDKCreateError& _errorCode );

}

#endif // __ZPV_UTILITIES_H__
