#include "stdafx.h"

namespace zpv
{

	TerrainManager::TerrainManager()
	{
        initDefaultTerrain();
	}

	NxActor* TerrainManager::defaultTerrain()
	{
		if( !m_Terrain )
			initDefaultTerrain();
		return m_Terrain;
	}

	void TerrainManager::initDefaultTerrain()
	{
		m_MaterialFactory = new MaterialFactory( PhysXManager::getSingletonRef().getDefaultScene() );

		for( int y = 0; y < TERRAIN_SIZE; y++ )
		{
			for( int x = 0; x < TERRAIN_SIZE; x++ )
			{
				NxVec3 v; 
				v.set(NxF32(x)-(NxF32(TERRAIN_SIZE-1)*0.5f), 0.0f, NxF32(y)-(NxF32(TERRAIN_SIZE-1)*0.5f));
				v *= TERRAIN_WIDTH;
				m_TerrainVerts[x+y*TERRAIN_SIZE] = v;
			}
		}

		struct Local
		{
			static void _Compute( bool* done,
				                  TerrainManager::TerrainVertsArray& field,
								  int x0,
								  int y0,
								  int size,
								  NxF32 value )
			{
				// Compute new size
				size >>= 1;
				if( !size )
					return;

				// Compute new heights
				NxF32 v0 = value*CustomRandom::trand();
				NxF32 v1 = value*CustomRandom::trand();
				NxF32 v2 = value*CustomRandom::trand();
				NxF32 v3 = value*CustomRandom::trand();
				NxF32 v4 = value*CustomRandom::trand();

				int x1 = ( x0 + size )		% TERRAIN_SIZE;
				int x2 = ( x0 + size + size )	% TERRAIN_SIZE;
				int y1 = ( y0 + size )		% TERRAIN_SIZE;
				int y2 = ( y0 + size + size )	% TERRAIN_SIZE;

				if(!done[x1 + y0*TERRAIN_SIZE])
					field[x1 + y0*TERRAIN_SIZE].y = v0 + 0.5f * (field[x0 + y0*TERRAIN_SIZE].y + field[x2 + y0*TERRAIN_SIZE].y);
				if(!done[x0 + y1*TERRAIN_SIZE])
					field[x0 + y1*TERRAIN_SIZE].y = v1 + 0.5f * (field[x0 + y0*TERRAIN_SIZE].y + field[x0 + y2*TERRAIN_SIZE].y);
				if(!done[x2 + y1*TERRAIN_SIZE])
					field[x2 + y1*TERRAIN_SIZE].y = v2 + 0.5f * (field[x2 + y0*TERRAIN_SIZE].y + field[x2 + y2*TERRAIN_SIZE].y);
				if(!done[x1 + y2*TERRAIN_SIZE])
					field[x1 + y2*TERRAIN_SIZE].y = v3 + 0.5f * (field[x0 + y2*TERRAIN_SIZE].y + field[x2 + y2*TERRAIN_SIZE].y);
				if(!done[x1 + y1*TERRAIN_SIZE])
					field[x1 + y1*TERRAIN_SIZE].y = v4 + 0.5f * (field[x0 + y1*TERRAIN_SIZE].y + field[x2 + y1*TERRAIN_SIZE].y);

				done[x1 + y0*TERRAIN_SIZE] = true;
				done[x0 + y1*TERRAIN_SIZE] = true;
				done[x2 + y1*TERRAIN_SIZE] = true;
				done[x1 + y2*TERRAIN_SIZE] = true;
				done[x1 + y1*TERRAIN_SIZE] = true;

				// Recurse through 4 corners
				value *= 0.5f;
				_Compute(done, field, x0,	y0,	size, value);
				_Compute(done, field, x0,	y1,	size, value);
				_Compute(done, field, x1,	y0,	size, value);
				_Compute(done, field, x1,	y1,	size, value);
			}
		};

		// Fractalize
		bool* done = new bool[TERRAIN_NB_VERTS];

		memset(done,0,TERRAIN_NB_VERTS);
		m_TerrainVerts[0].y = 10.0f;
		m_TerrainVerts[TERRAIN_SIZE-1].y = 10.0f;
		m_TerrainVerts[TERRAIN_SIZE*(TERRAIN_SIZE-1)].y = 5.0f;
		m_TerrainVerts[TERRAIN_NB_VERTS-1].y = 10.0f;
		Local::_Compute(done, m_TerrainVerts, 0, 0, TERRAIN_SIZE, TERRAIN_CHAOS);
		for(int i=0;i<TERRAIN_NB_VERTS;i++)
		{
			m_TerrainVerts[i].y += TERRAIN_OFFSET;

			//clamp terrain to positives to create a flat area for the vehicles to spawn on
			if (i % TERRAIN_SIZE > TERRAIN_SIZE*0.5f && i < TERRAIN_NB_VERTS*0.8f && i > TERRAIN_NB_VERTS*0.3f)
				m_TerrainVerts[i].y = 0.0f;
		}

		delete[] done;
		

		// Initialize terrain faces
		//m_TerrainFaces = new int[TERRAIN_NB_FACES*3];

		int k = 0;
		for(int j=0;j<TERRAIN_SIZE-1;j++)
		{
			for(int i=0;i<TERRAIN_SIZE-1;i++)
			{
				// Create first triangle
				m_TerrainFaces[k] = i   + j*TERRAIN_SIZE;
				m_TerrainFaces[k+1] = i   + (j+1)*TERRAIN_SIZE;
				m_TerrainFaces[k+2] = i+1 + (j+1)*TERRAIN_SIZE;

				//while we're at it do some smoothing of the random terrain because its too rough to do a good demo of this effect.
				//smoothTriangle(m_TerrainFaces[k],m_TerrainFaces[k+1],m_TerrainFaces[k+2]);
				k+=3;
				// Create second triangle
				m_TerrainFaces[k] = i   + j*TERRAIN_SIZE;
				m_TerrainFaces[k+1] = i+1 + (j+1)*TERRAIN_SIZE;
				m_TerrainFaces[k+2] = i+1 + j*TERRAIN_SIZE;

				//smoothTriangle(m_TerrainFaces[k],m_TerrainFaces[k+1],m_TerrainFaces[k+2]);
				k+=3;
			}
		}

		//allocate terrain materials -- one for each face.
		m_TerrainMaterials = new NxMaterialIndex[TERRAIN_NB_FACES];

		NxReal lastSteepness = 0.0f;
		for(int f=0;f<TERRAIN_NB_FACES;f++)
		{
			//new: generate material indices for all the faces
			lastSteepness = chooseTrigMaterial(f, lastSteepness);
		}
		// Build vertex normals
		NxBuildSmoothNormals( TERRAIN_NB_FACES,
			                  TERRAIN_NB_VERTS,
							  m_TerrainVerts.data(),
							  m_TerrainFaces.data(),
							  NULL,
							  m_TerrainNormals.data(),
							  true );

		// Build physical model
		NxTriangleMeshDesc terrainDesc;
		terrainDesc.numVertices					= TERRAIN_NB_VERTS;
		terrainDesc.numTriangles				= TERRAIN_NB_FACES;
		terrainDesc.pointStrideBytes			= sizeof(NxVec3);
		terrainDesc.triangleStrideBytes			= 3*sizeof(int);
		terrainDesc.points						= m_TerrainVerts.data();
		terrainDesc.triangles					= m_TerrainFaces.data();							
		terrainDesc.flags						= 0;
		//add the mesh material data:
		terrainDesc.materialIndexStride			= sizeof(NxMaterialIndex);
		terrainDesc.materialIndices				= m_TerrainMaterials;

		terrainDesc.heightFieldVerticalAxis		= NX_Y;
		terrainDesc.heightFieldVerticalExtent	= -1000.0f;

		NxTriangleMeshShapeDesc terrainShapeDesc;
		CookingManager::getSingletonRef().initCooking();
 		MemoryWriteBuffer buf;
		bool status = CookingManager::getSingletonRef().cookTriangleMesh( terrainDesc, buf );
  		MemoryReadBuffer readBuffer(buf.data);
  		terrainShapeDesc.meshData = PhysXManager::getSingletonRef().getSDK()->createTriangleMesh(readBuffer);
		//
		// Please note about the created Triangle Mesh, user needs to release it when no one uses it to save memory. It can be detected
		// by API "meshData->getReferenceCount() == 0". And, the release API is "PhysXManager::getSingletonRef().getSDK()->releaseTriangleMesh(*meshData);"
		//
		NxActorDesc actorDesc;
		actorDesc.shapes.pushBack(&terrainShapeDesc);
		m_Terrain = PhysXManager::getSingletonRef().getDefaultScene()->createActor(actorDesc);
		m_Terrain->userData = (void*)0;
		CookingManager::getSingletonRef().closeCooking();
		delete m_MaterialFactory;
	}

	void TerrainManager::smoothTriangle( int a, int b, int c )
	{
		NxVec3 & v0 = m_TerrainVerts[a];
		NxVec3 & v1 = m_TerrainVerts[b];
		NxVec3 & v2 = m_TerrainVerts[c];

		NxReal avg = (v0.y + v1.y + v2.y) * 0.333f;
		avg *= 0.5f;
		v0.y = v0.y * 0.5f + avg;
		v1.y = v1.y * 0.5f + avg;
		v2.y = v2.y * 0.5f + avg;
	}

	NxReal TerrainManager::chooseTrigMaterial(int faceIndex, NxReal lastSteepness)
	{
		NxVec3 & v0 = m_TerrainVerts[m_TerrainFaces[faceIndex * 3]];
		NxVec3 & v1 = m_TerrainVerts[m_TerrainFaces[faceIndex * 3 + 1]];
		NxVec3 & v2 = m_TerrainVerts[m_TerrainFaces[faceIndex * 3 + 2]];

		NxVec3 edge0 = v1 - v0;
		NxVec3 edge1 = v2 - v0;

		NxVec3 normal = edge0.cross(edge1);
		normal.normalize();
		NxReal steepness = 1.0f - normal.y;
		steepness += lastSteepness;
		steepness /= 2.0f;

		if(steepness > 0.25f)
		{
			m_TerrainMaterials[faceIndex] = m_MaterialFactory->getIce();
		}
		else if(steepness > 0.2f)
		{
			m_TerrainMaterials[faceIndex] = m_MaterialFactory->getRock();
		}
		else if(steepness > 0.1f)
		{
			m_TerrainMaterials[faceIndex] = m_MaterialFactory->getMud();
		}
		else
			m_TerrainMaterials[faceIndex] = m_MaterialFactory->getGrass();
		return steepness;
	}
}