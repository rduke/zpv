#ifndef __ZPV_VEHICLE_MOTOR_H__
#define __ZPV_VEHICLE_MOTOR_H__

#include "ZpvVehicleMotorDesc.h"
#include "ZpvVehicleGears.h"

namespace zpv
{

	class VehicleMotor
	{
	public:
		VehicleMotor() : m_Rpm(0) { }
		static VehicleMotor* createMotor(const VehicleMotorDesc& _motorDesc);

		void		setRpm( NxReal _rpm ) { m_Rpm = _rpm; }
		NxReal		getRpm() const { return m_Rpm; }

		NxReal		getMinRpm() const { return m_MinRpm; }
		NxReal		getMaxRpm() const { return m_MaxRpm; }
		NxI32		changeGears( const VehicleGears* _gears, NxReal _threshold ) const;

		NxReal		getTorque() const { return m_TorqueCurve.getValue( m_Rpm ); }
	private:
		LinearInterpolationValues   m_TorqueCurve;
		NxReal						m_Rpm;
		NxReal						m_MaxTorque;
		NxReal						m_MaxTorquePos;
		NxReal						m_MaxRpmToGearUp;
		NxReal						m_MinRpmToGearDown;
		NxReal						m_MaxRpm;
		NxReal						m_MinRpm;
	};

	NX_INLINE VehicleMotor* VehicleMotor::createMotor( const VehicleMotorDesc& _motorDesc )
	{
		if ( !_motorDesc.isValid() )
			return NULL;

		VehicleMotor* motor = new VehicleMotor();
		motor->m_TorqueCurve = _motorDesc.torqueCurve;
		NxReal maxTorque = 0;
		NxI32 maxTorquePos = -1;
		for ( int i = 0; i < motor->m_TorqueCurve.getSize(); i++ )
		{
			NxReal v = motor->m_TorqueCurve.getValueAtIndex( i );
			if ( v > maxTorque )
			{
				maxTorque = v;
				maxTorquePos = i;
			}
		}
		motor->m_MaxTorque			= maxTorque;
		motor->m_MaxTorquePos		= ( NxReal )maxTorquePos;
		motor->m_MaxRpmToGearUp		= _motorDesc.maxRpmToGearUp;
		motor->m_MinRpmToGearDown	= _motorDesc.minRpmToGearDown;
		motor->m_MaxRpm				= _motorDesc.maxRpm;
		motor->m_MinRpm				= _motorDesc.minRpm;
		return motor;
	}

	NX_INLINE NxI32 VehicleMotor::changeGears( const VehicleGears* _gears,
		                                       NxReal _threshold ) const
	{
		NxI32 gear = _gears->getGear();
		if (m_Rpm > m_MaxRpmToGearUp && gear < _gears->getMaxGear() )
			return 1;
		else if (m_Rpm < m_MinRpmToGearDown && gear > 1)
			return -1;

		return 0;
	}

}

#endif // __ZPV_VEHICLE_MOTOR_H__