#ifndef __ZPV_ERROR_STREAM_H__
#define __ZPV_ERROR_STREAM_H__

#ifdef WIN32
#define NOMINMAX
#include <windows.h>
#endif

namespace zpv
{

	class ErrorStream
		: public NxUserOutputStream
	{
	public:
		void reportError( NxErrorCode _e,
			              const char* _message,
						  const char* _file,
						  int         _line )
		{
			printf( "%s (%d) :", _file, _line );
			switch ( _e )
			{
				case NXE_INVALID_PARAMETER:
					printf( "invalid parameter" );
					break;
				case NXE_INVALID_OPERATION:
					printf( "invalid operation" );
					break;
				case NXE_OUT_OF_MEMORY:
					printf( "out of memory" );
					break;
				case NXE_DB_INFO:
					printf( "info" );
					break;
				case NXE_DB_WARNING:
					printf( "warning" );
					break;
				default:
					printf( "unknown error" );
			}

			printf( " : %s\n", _message );
		}

		NxAssertResponse reportAssertViolation( const char* _message,
			                                    const char* _file, 
												int         _line )
		{
			printf( "access violation : %s (%s line %d)\n", _message, _file, _line );
	#ifdef WIN32
			switch ( MessageBox( 0, (LPCSTR)_message, (LPCSTR)"AssertViolation, see console for details.", MB_ABORTRETRYIGNORE ) )
			{
				case IDRETRY:
					return NX_AR_CONTINUE;
				case IDIGNORE:
					return NX_AR_IGNORE;
				case IDABORT:
				default:
					return NX_AR_BREAKPOINT;
			}
	#elif LINUX
			assert(0);
	#elif _XBOX
			return NX_AR_BREAKPOINT;
	#elif __CELLOS_LV2__
			return NX_AR_BREAKPOINT;
	#endif
		}

		void print( const char* _message )
		{
			printf( _message );
		}
	};
}

#endif // __ZPV_ERROR_STREAM_H__