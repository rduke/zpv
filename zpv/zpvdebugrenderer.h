

#ifndef __ZPV_DEBUGRENDER_H__
#define __ZPV_DEBUGRENDER_H__

#include "NxDebugRenderable.h"
#include "ZpvSingleton.h"
#include "ZpvPhysXManager.h"
#include <GL>

class NxDebugRenderable;

namespace zpv
{

	class DebugRenderer
		: public Singleton< DebugRenderer >
	{
	public:
		void renderData( const NxDebugRenderable& _data = 
			             *( PhysXManager::getSingletonRef().getDefaultScene()->getDebugRenderable() ) ) const;

	private:
		static void renderBuffer( float* _pVertList,
			                      float* _pColorList,
								  int    _type,
								  int    _num );
	};

}

#endif // __ZPV_DEBUGRENDER_H__

