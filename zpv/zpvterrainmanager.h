#ifndef __ZPV_TERRAIN_MANAGER_H__
#define __ZPV_TERRAIN_MANAGER_H__

#include "ZpvSingleton.h"
#include "ZpvPhysXManager.h"
#include "ZpvCustomRandom.h"
#include "ZpvMaterialFactory.h"
#include <boost/array.hpp>

#define TERRAIN_SIZE		33
#define TERRAIN_NB_VERTS	TERRAIN_SIZE*TERRAIN_SIZE
#define TERRAIN_NB_FACES	(TERRAIN_SIZE-1)*(TERRAIN_SIZE-1)*2
#define TERRAIN_OFFSET		0.0f
#define TERRAIN_WIDTH		20.0f
#define TERRAIN_CHAOS		70.0f //150.0f

namespace zpv
{
	class TerrainManager
		: public Singleton< TerrainManager >
	{
	public:
		NxActor* defaultTerrain();
		void     initDefaultTerrain();
		typedef boost::array< NxVec3, TERRAIN_NB_VERTS > TerrainVertsArray;
		typedef boost::array< NxU32, TERRAIN_NB_FACES * 3 >  TerrainFacesArray;
		typedef boost::array< NxVec3, TERRAIN_NB_VERTS > TerrainNormalsArray;
		const TerrainVertsArray& getTerrainVerts() const { return m_TerrainVerts; }
		const TerrainFacesArray& getTerrainFaces() const { return m_TerrainFaces; }
		const TerrainNormalsArray& getTerrainNormals() const { return m_TerrainNormals; }
		const NxMaterialIndex*     getTerrainMaterials() const { return m_TerrainMaterials; }

	protected:
		friend class Singleton< TerrainManager >;
		explicit TerrainManager();

	private:
		void smoothTriangle( int a, int b, int c );
		NxReal chooseTrigMaterial(int faceIndex, NxReal lastSteepness);
		NxActor* m_Terrain;
		TerrainVertsArray m_TerrainVerts;
		TerrainFacesArray m_TerrainFaces;
		TerrainNormalsArray m_TerrainNormals;
		NxMaterialIndex* m_TerrainMaterials;
		MaterialFactory* m_MaterialFactory;
	};
}

#endif // __ZPV_TERRAIN_MANAGER_H__