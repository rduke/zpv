#ifndef __ZPV_TEST_VEHICLE_H__
#define __ZPV_TEST_VEHICLE_H__

#include "ZpvTestVehicleDesc.h"
#include "ZpvWheel.h"
#include <NxScene.h>
#include <NxArray.h>
#include <NxUserContactReport.h>
#include "ZpvAbstractVehicle.h"

#define NUM_TRAIL_POINTS 1600

namespace zpv
{
	
	class TestVehicle
		: public AbstractVehicle
	{
	public:
		NxArray< Wheel* >		m_Wheels;
		NxArray<TestVehicle*>	m_Children;
		NxActor*				m_BodyActor;
		NxScene*				m_Scene;
		
		std::auto_ptr< Zmvs::VehicleAutoGearBoxMotion > m_Motion;

		NxReal					m_SteeringWheelState;
		NxReal					m_AccelerationPedal;
		NxReal					m_BrakePedal;
		bool					m_BrakePedalChanged;
		bool					m_HandBrake;

		NxReal					m_DigitalSteeringDelta;
		NxVec3					m_SteeringTurnPoint;
		NxVec3					m_SteeringSteerPoint;
		NxReal					m_SteeringMaxAngleRad;
		NxReal					m_MotorForce;
		NxReal					m_TransmissionEfficiency;
		NxReal					m_DifferentialRatio;

		NxVec3					m_LocalVelocity;
		bool					m_Braking;
		bool					m_ReleaseBraking;
		NxReal					m_MaxVelocity;
		NxMaterial*				m_CarMaterial;
		NxReal					m_CameraDistance;

		NxVec3					m_TrailBuffer[ NUM_TRAIL_POINTS ];
		int					    m_NextTrailSlot;
		NxReal					m_LastTrailTime;

		NxActor*				m_MostTouchedActor;

		void					computeMostTouchedActor();
		void					computeLocalVelocity();
		NxReal					computeAxisTorque();
		NxReal					computeRpmFromWheels();
		NxReal					computeMotorRpm(NxReal rpm);

		void					updateRpms();

		NxReal					getGearRatio();

		void					controlSteering(NxReal steering, bool analogSteering);
		void					controlAcceleration(NxReal acceleration, bool analogAcceleration);
		static TestVehicle*		    _createTestVehicle(NxScene* scene, TestVehicleDesc* vehicleDesc);
	public:
		void*					userData;



		TestVehicle();
		~TestVehicle();

		void					handleContactPair(NxContactPair& pair, int carIndex);
		void					updateVehicle(NxReal lastTimeStepSize);
		void					control (NxReal steering, bool analogSteering, NxReal acceleration, bool analogAcceleration, bool handBrake);
		void					gearUp();
		void					gearDown();

		void					draw(bool debug = false);

		void					applyRandomForce();
		void					standUp();

		NxReal					getDriveVelocity() { return NxMath::abs(m_LocalVelocity.x); }

		NxReal					getMaxVelocity() { return m_MaxVelocity; }
		NxActor*				getActor() { return m_BodyActor; }
		TestVehicle*				getChild(NxU32 i);
		void					addChild(TestVehicle* child);
		int					nbChildren() { return m_Children.size(); }

		int					getNbWheels() { return m_Wheels.size(); }
		const Wheel*			getWheel(NxU32 i) { NX_ASSERT(i < m_Wheels.size()); return m_Wheels[i]; }
		NxReal					getCameraDistance() { return m_CameraDistance; }

		NxMat34 getGlobalPose() { return m_BodyActor->getGlobalPose(); }

		static TestVehicle* createTestVehicle(NxScene* scene, TestVehicleDesc* vehicleDesc);
	};

	NX_INLINE TestVehicle* TestVehicle::getChild(NxU32 i)
	{
		if (i < m_Children.size())
			return m_Children[i];
		return NULL;
	}

	NX_INLINE void TestVehicle::addChild(TestVehicle* child)
	{
		m_Children.pushBack(child);
	}

}

#endif // __ZPV_TEST_VEHICLE_H__