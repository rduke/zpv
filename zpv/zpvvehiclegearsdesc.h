#ifndef __ZPV_VEHICLE_GEARS_DESC_H__
#define __ZPV_VEHICLE_GEARS_DESC_H__

#include "ZpvLinearInterpolationValues.h"
#include <NxArray.h>

#define NX_VEHICLE_MAX_NB_GEARS 32

namespace zpv
{

	class VehicleGearsDesc 
	{
	public:
		VehicleGearsDesc() { setToDefault(); }
		void setToDefault();
		void setToCorvette();
		bool isValid() const;
		int									getMaxNumOfGears() const { return NX_VEHICLE_MAX_NB_GEARS; }


		int									nbForwardGears;
		float								forwardGearRatios[NX_VEHICLE_MAX_NB_GEARS];
		float								backwardGearRatio;
	};

	NX_INLINE void VehicleGearsDesc::setToDefault()
	{
	}

	NX_INLINE void VehicleGearsDesc::setToCorvette()
	{
		forwardGearRatios[0] = 2.66f;
		forwardGearRatios[1] = 1.78f;
		forwardGearRatios[2] = 1.30f;
		forwardGearRatios[3] = 1;
		forwardGearRatios[4] = 0.74f;
		forwardGearRatios[5] = 0.50f;
		nbForwardGears = 6;

		backwardGearRatio = -2.90f;

	}

	inline bool VehicleGearsDesc::isValid() const
	{
		if (nbForwardGears > getMaxNumOfGears())
		{
			fprintf(stderr, "VehicleGearsDesc::isValid(): nbForwardGears(%d) is bigger than max (%d)\n",
				nbForwardGears, getMaxNumOfGears());
			return false;
		}
		if (nbForwardGears <= 0)
		{
			fprintf(stderr, "VehicleGearsDesc::isValid(): nbForwardGears(%d) smaller or equal 0\n", nbForwardGears);
			return false;
		}
		if (backwardGearRatio > 0)
		{
			fprintf(stderr, "NxVehilceGearDesc::isValid(): backwardGearRatio(%2.3f) is bigger than 0, make it negative\n", backwardGearRatio);
			return false;
		}
		for (int i = 0; i < nbForwardGears; i++)
		{
			if (forwardGearRatios[i] < 0)
			{
				fprintf(stderr, "NxVehilceGearDesc::isValid(): forwardGearRatios[%d] (%2.3f) has value smaller 0\n", i, forwardGearRatios[i]);
				return false;
			}
		}
		return true;
	}
}

#endif // __ZPV_VEHICLE_GEARS_DESC_H__
