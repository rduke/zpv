#ifndef __ZPV_SHAPE_WHEEL_H__
#define __ZPV_SHAPE_WHEEL_H__

#include "ZpvWheel.h"

namespace zpv
{
	class ShapeWheel
		: public Wheel
	{
	public:
		ShapeWheel( NxActor* actor, WheelDesc* wheelDesc );
		virtual					~ShapeWheel();

		virtual void			tick( bool handbrake,
			                          NxReal motorTorque,
									  NxReal brakeTorque,
									  NxReal dt );

		virtual NxActor *		getTouchedActor() const;
		virtual NxVec3			getWheelPos() const;
		virtual void			setAngle(NxReal angle);
		virtual void			drawWheel(NxReal approx, bool debug = false) const;
		virtual NxReal			getRpm() const;
		virtual NxVec3			getGroundContactPos() const { return getWheelPos()+NxVec3(0, -wheelShape->getRadius(), 0); }
		virtual float			getRadius() const { return wheelShape->getRadius(); }

	private:
		NxActor* actor;
		NxWheelShape * wheelShape;
	};
}

#endif // __ZPV_SHAPE_WHEEL_H__