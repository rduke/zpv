#ifndef __ZPV_VEHICLE_MANAGER_H__
#define __ZPV_VEHICLE_MANAGER_H__

#include <set>
#include "ZpvAbstractVehicle.h"
#include "ZpvInputManager.h"

namespace zpv
{
	class VehicleManager 
	{
	private:
		static std::set<AbstractVehicle*> _allVehicles;
		static NxArray<AbstractVehicle*> _allVehiclesSequential;
		static std::set<AbstractVehicle*> _allChildVehicles;
		
		static NxI32 _activeVehicle;
		static AbstractVehicle* _activeVehicleP;
		static void setActiveVehicleP();
	public:
		static void AddVehicle(AbstractVehicle* v);
		static void AddChildVehicle(AbstractVehicle* v);

		static void updateAllVehicles(NxReal lastTimeStep);
		static void drawVehicles( bool debug = InputManager::getSingletonRef().isDebugVisualization() );

		static bool isVehicle(AbstractVehicle* v);
		static void handlePair(NxContactPair& pair, int events);

		static int getNumberOfVehicles() { return (int)_allVehicles.size(); }
		static void setActiveVehicle(NxI32 v) { _activeVehicle = v; setActiveVehicleP(); }
		static AbstractVehicle* getActiveVehicle() { return _activeVehicleP; }
		static NxI32 getActiveVehicleNumber() { return _activeVehicle; }
		static void selectNext();

	};
}

#endif // __ZPV_VEHICLE_MANAGER_H__
