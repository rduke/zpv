#ifndef __ZPV_COOKING_H__
#define __ZPV_COOKING_H__

#include "NxCooking.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "NxPhysicsSDK.h"
#include "NxPMap.h"
#include "PhysXLoader.h"
#include "ZpvSingleton.h"

class NxPMap;
class NxTriangleMesh;
class NxUserOutputStream;

namespace zpv
{

	class CookingManager
		: public Singleton< CookingManager >
	{
	public:
		bool hasCookingLibrary(); // check to see if the cooking library is available or not!
		bool initCooking( NxUserAllocator* allocator = NULL,
			              NxUserOutputStream* outputStream = NULL );

		void closeCooking();
		bool cookConvexMesh( const NxConvexMeshDesc& desc,
			                       NxStream&         stream );

		bool cookClothMesh( const NxClothMeshDesc& desc,
			                      NxStream&        stream );

		bool cookTriangleMesh( const NxTriangleMeshDesc& desc,
			                         NxStream&           stream );

		bool cookSoftBodyMesh( const NxSoftBodyMeshDesc& desc,
			                         NxStream&           stream );

		bool createPMap( NxPMap& pmap,
			       const NxTriangleMesh& mesh,
				         int density,
						 NxUserOutputStream* outputStream = NULL );

		bool releasePMap( NxPMap& pmap );
	};

}

#endif // __ZPV_COOKING_H__
