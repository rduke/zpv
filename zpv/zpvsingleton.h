#ifndef __ZPV_SINGLETON_H__
#define __ZPV_SINGLETON_H__

#include <cassert>

namespace zpv
{

	template<typename T>
	class Singleton
	{
	public: 
		static T& getSingletonRef();
		static T* getSingleton();
		static void destroy();

	protected: 
		inline explicit Singleton()
		{ 
			assert(Singleton::ms_Instance == 0); 
			Singleton::ms_Instance = static_cast<T*>(this); 
		}
		inline virtual ~Singleton()
		{ 
			Singleton::ms_Instance = 0; 
		}

	private: 
		static T* createInstance();
		static void scheduleForDestruction(void (*)());
		static void destroyInstance(T*);

	private : 
		static T* ms_Instance;

	private : 
		inline explicit Singleton(Singleton const&) {}
		inline Singleton& operator=(Singleton const&) { return *this; }
	};    //    end of class Singleton


	template<typename T>
	typename T& Singleton<T>::getSingletonRef()
	{
		if ( Singleton::ms_Instance == 0 )
		{
			Singleton::ms_Instance = Singleton::createInstance();
			scheduleForDestruction(Singleton::destroy);
		}
		return *(Singleton::ms_Instance);
	}

	template<typename T>
	typename T* Singleton<T>::getSingleton()
	{
		if ( Singleton::ms_Instance == 0 )
		{
			Singleton::ms_Instance = createInstance();
			scheduleForDestruction(Singleton::destroy);
		}
		return Singleton::ms_Instance;
	}

	template<typename T>
	void Singleton<T>::destroy()
	{
		if ( Singleton::ms_Instance != 0 ) 
		{
			destroyInstance(Singleton::ms_Instance);
			Singleton::ms_Instance = 0;
		}
	}

	template<typename T>
	inline typename T* Singleton<T>::createInstance()
	{
		return new T();
	}

	template<typename T>
	inline void Singleton<T>::scheduleForDestruction(void (*pFun)())
	{
		atexit( pFun );
	}

	template<typename T>
	inline void Singleton<T>::destroyInstance(T* p)
	{
		delete p;
	}

	template<typename T>
	typename T* Singleton<T>::ms_Instance = 0;

}

#endif // __ZPV_SINGLETON_H__