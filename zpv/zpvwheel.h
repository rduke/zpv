#ifndef __WHEEL_H__
#define __WHEEL_H__

#include <cstdio>
#include <GL>

#include "ZpvWheelDesc.h"
#include "ZpvContactInfo.h"


namespace zpv
{
	class Vehicle;

	class Wheel
	{
	public:
		static Wheel* createWheel( NxActor* actor, WheelDesc* wheelDesc );

		virtual					~Wheel() {}
		virtual void			tick( bool handbrake,
			                          NxReal motorTorque,
									  NxReal brakeTorque,
									  NxReal dt ) = 0;

		virtual NxActor *		getTouchedActor() const = 0;
		virtual NxVec3			getWheelPos() const = 0;
		virtual void			setAngle( NxReal angle ) = 0;
		virtual void			drawWheel( NxReal approx,
			                               bool debug = false ) const = 0;

		virtual NxReal			getRpm() const = 0;
		virtual NxVec3			getGroundContactPos() const = 0;
		virtual float			getRadius() const = 0;

		NX_INLINE bool			hasGroundContact() const 
		{
			return getTouchedActor() != NULL;
		}
		NX_INLINE bool			getWheelFlag( NxWheelFlags flag ) const 
		{
			return ( wheelFlags & flag ) != 0;
		}
		void*					userData;

	protected:
		int					wheelFlags;
	};

}


#endif // __WHEEL_H__