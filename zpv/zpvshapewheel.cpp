#include "stdafx.h"

namespace zpv
{

ShapeWheel::ShapeWheel( NxActor* a, WheelDesc* wheelDesc ) 
	: actor( a )
{

	NxScene * scene = &actor->getScene();
	//create a shared car wheel material to be used by all wheels
	static NxMaterial * wsm = 0;
	if( !wsm )
	{
		NxMaterialDesc m;
		m.flags |= NX_MF_DISABLE_FRICTION;
		wsm = scene->createMaterial( m );
	}

	NxWheelShapeDesc wheelShapeDesc;

	wheelShapeDesc.localPose.t = wheelDesc->position;
	NxQuat q;
	q.fromAngleAxis(90.0f, NxVec3(0,1,0));
	wheelShapeDesc.localPose.M.fromQuat(q);
	wheelShapeDesc.materialIndex = wsm->getMaterialIndex();
	wheelFlags = wheelDesc->wheelFlags;



	NxReal heightModifier = ( wheelDesc->wheelSuspension + wheelDesc->wheelRadius )
		                     / wheelDesc->wheelSuspension;

	wheelShapeDesc.suspension.spring = wheelDesc->springRestitution*heightModifier;
	wheelShapeDesc.suspension.damper = 0;
	wheelShapeDesc.suspension.targetValue = wheelDesc->springBias*heightModifier;

	wheelShapeDesc.radius = wheelDesc->wheelRadius;
	wheelShapeDesc.suspensionTravel = wheelDesc->wheelSuspension; 
	wheelShapeDesc.inverseWheelMass = 0.1f;	//not given!? TODO

	wheelShapeDesc.lateralTireForceFunction.stiffnessFactor *= wheelDesc->frictionToSide;	
	wheelShapeDesc.longitudalTireForceFunction.stiffnessFactor *= wheelDesc->frictionToFront;	

	wheelShape = static_cast<NxWheelShape *>(actor->createShape(wheelShapeDesc));
}

ShapeWheel::~ShapeWheel()
{
}

void ShapeWheel::tick(bool handBrake, NxReal motorTorque, NxReal brakeTorque, NxReal dt)
{
	brakeTorque *= 500.0f;
	if(handBrake && getWheelFlag(NX_WF_AFFECTED_BY_HANDBRAKE))
		brakeTorque = 1000.0f;

	if(getWheelFlag(NX_WF_ACCELERATED)) 
		wheelShape->setMotorTorque(motorTorque);

	wheelShape->setBrakeTorque(brakeTorque);
}

NxActor* ShapeWheel::getTouchedActor() const
{
	NxWheelContactData wcd;
	NxShape * s = wheelShape->getContact(wcd);	
	return s ? &s->getActor() : 0;
}

NxVec3 ShapeWheel::getWheelPos() const
{
	return wheelShape->getLocalPosition();
}

void ShapeWheel::setAngle(NxReal angle)
{
	wheelShape->setSteerAngle(-angle);
}

void ShapeWheel::drawWheel(NxReal approx, bool debug) const
{
	//nothing, taken care of by built in visualization.
	NxWheelContactData wcd;
	NxShape* s = wheelShape->getContact(wcd);	
	if( !s ) 
		return;

}

NxReal ShapeWheel::getRpm() const
{
	return NxMath::abs(wheelShape->getAxleSpeed())/NxTwoPi * 60.0f;
}

}