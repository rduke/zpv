#ifndef __CONTACT_INFO_H__
#define __CONTACT_INFO_H__

#include <NxPhysics.h>

namespace zpv
{
	
	class ContactInfo 
	{
	public:
		ContactInfo() 
		{
			reset();
		}
		void reset() 
		{
			otherActor = NULL;
			relativeVelocity = 0;
		}
		bool isTouching() const 
		{
			return otherActor != NULL;
		}
		NxActor*				otherActor;
		NxVec3					contactPosition;
		NxVec3					contactPositionLocal;
		NxVec3					contactNormal;
		NxReal					relativeVelocity;
		NxReal					relativeVelocitySide;
	};

}

#endif //  __CONTACT_INFO_H__