#ifndef __ZPV_ABSTRACT_VEHICLE_H__
#define __ZPV_ABSTRACT_VEHICLE_H__

namespace zpv
{

	class AbstractVehicle
	{
	public:
		virtual void gearUp() = 0;
		virtual void gearDown() = 0;
		virtual void standUp()  = 0;
		virtual void updateVehicle( float _lastTimeStep ) = 0;
		virtual void draw( bool _debug ) = 0;
		virtual void handleContactPair(NxContactPair& pair, int carIndex) = 0;
		virtual NxMat34 getGlobalPose() = 0;
		virtual NxReal getCameraDistance() = 0;
		virtual void control( NxReal _steering,
			                  bool   _analogSteering,
							  NxReal _acceleration,
							  bool   _analogAcceleration,
							  bool   _handBrake ) = 0;

		virtual ~AbstractVehicle() { }
	};

}

#endif // __ZPV_ABSTRACT_VEHICLE_H__