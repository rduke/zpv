#include "stdafx.h"

namespace zpv
{

	const char* getNxSDKCreateError( const NxSDKCreateError& _errorCode ) 
	{
		switch( _errorCode ) 
		{
			case NXCE_NO_ERROR: 
				return "NXCE_NO_ERROR";
			case NXCE_PHYSX_NOT_FOUND: 
				return "NXCE_PHYSX_NOT_FOUND";
			case NXCE_WRONG_VERSION: 
				return "NXCE_WRONG_VERSION";
			case NXCE_DESCRIPTOR_INVALID: 
				return "NXCE_DESCRIPTOR_INVALID";
			case NXCE_CONNECTION_ERROR: 
				return "NXCE_CONNECTION_ERROR";
			case NXCE_RESET_ERROR: 
				return "NXCE_RESET_ERROR";
			case NXCE_IN_USE_ERROR: 
				return "NXCE_IN_USE_ERROR";
			default: 
				return "Unknown error";
		}
	};

}