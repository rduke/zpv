#include "stdafx.h"

namespace zpv
{

	MaterialFactory::MaterialFactory( NxScene* _scene )
		: m_Scene( _scene ),
	       m_Rock( 0      ),
		    m_Ice( 0      ),
		  m_Grass( 0      ),
		    m_Mud( 0      )
	{
        NxMaterialDesc	materialDesc;

		NxMaterial* defaultMaterial = m_Scene->getMaterialFromIndex( 0 ); 
		defaultMaterial->setRestitution( 0.0f );
		defaultMaterial->setStaticFriction( 0.5f );
		defaultMaterial->setDynamicFriction( 0.5f );

		// Ice
		materialDesc.restitution		= 1.0f;
		materialDesc.staticFriction 	= 0.05f;
		materialDesc.dynamicFriction	= 0.05f;
		m_Ice = m_Scene->createMaterial( materialDesc )->
			                                   getMaterialIndex();

		// Rock
		materialDesc.restitution		= 0.3f;
		materialDesc.staticFriction 	= 1.2f;
		materialDesc.dynamicFriction	= 0.9f;
		m_Rock = m_Scene->createMaterial( materialDesc )->
			                                    getMaterialIndex();
		// Mud
		materialDesc.restitution		= 0.0f;
		materialDesc.staticFriction 	= 0.8f;
		materialDesc.dynamicFriction	= 0.2f;
		m_Mud = m_Scene->createMaterial( materialDesc )->
			                                    getMaterialIndex();
		// Grass
		materialDesc.restitution		= 0.1f;
		materialDesc.staticFriction  	= 0.4f;
		materialDesc.dynamicFriction	= 0.4f;
		m_Grass = m_Scene->createMaterial( materialDesc )->
			                                     getMaterialIndex();
	}

}