#ifndef __ZPV_INPUT_MANAGER_H__
#define __ZPV_INPUT_MANAGER_H__

#include "ZpvSingleton.h"

namespace zpv
{

    class InputManager
		: public Singleton< InputManager >
	{
	public:
		friend class Singleton< InputManager >;
		void appKey( unsigned char _key, bool _down );
		void callback_key( unsigned char c, int x, int y );
		void callback_keyUp( unsigned char c, int x, int y );
		void getCurrentPosAndDirection( NxVec3& pos, NxVec3& direction );
		void updateCamera( NxVec3& _cameraPos, NxVec3& _cameraDir );
		void createCube(const NxVec3& pos, const NxVec3* initial_velocity);
		bool getPause() const { return gPause; }
		bool isDebugVisualization() const { return gDebugVisualization; }
		void simulate( float _timeStep = 1.0f / 30.0f );
		void cameraControls();
		const NxVec3& getEye() const { return Eye; }
		const NxVec3& getDir() const { return Dir; }

	protected:
		explicit InputManager()
			: gLastVehicleNumber( 0 ),
			  gDebugVisualization( false ),
              gPause( false ),
			  Eye(250, 50, 65),
              Dir(-0.65,-0.53,-0.54),
              CameraPos(250, 50, 65),
              CameraDir(-0.65, -0.53, -0.54)
		{
			for( int i = 0; i < 256; i++ )
			{
				keyDown[ i ] = false;
			}
		}

	private:
		NxVec3              Eye;
		NxVec3              Dir;
		NxVec3              CameraPos;
		NxVec3              CameraDir;
		bool                keyDown[256];
		bool				gPause;
		bool				gDebugVisualization;
		NxI32               gLastVehicleNumber;
	};

}

#endif // __ZPV_INPUT_MANAGER_H__